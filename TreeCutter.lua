local args = {...}
local Robot = require("Robot")

-- VARIABLE INTRODUCED AS OF VERSION 0.1
TreeCutter = { VERSION = 0.1, ROBOT_VERSION = 0.27 }

-- VARIABLE INTRODUCED AS OF VERSION 0.1
TreeCutter.Error = nil

-- VARIABLE INTRODUCED AS OF VERSION 0.1
TreeCutter.Module = nil

-- VARIABLE INTRODUCED AS OF VERSION 0.1
TreeCutter.ModuleDirectory = "TreeCutterModules"

-- VARIABLE INTRODUCED AS OF VERSION 0.1
TreeCutter.ModuleFile = nil

-- HELPER FUNCTION INTRODUCED AS OF VERSION 0.1
function TreeCutter.IsLua(path)
	local ext = path:match("^.+(%..+)$")
	return ext == ".lua"
end

-- HELPER FUNCTION INTRODUCED AS OF VERSION 0.1
function TreeCutter.GetModuleName(path)
	local module_name = path:sub(0, #path - 4)
	return module_name:gsub("%/", ".")
end

-- HELPER FUNCTION INTRODUCED AS OF VERSION 0.1
function TreeCutter.LoadModule(path)
	if fs.exists(path) then
		if TreeCutter.IsLua(path) then
			local module_name = TreeCutter.GetModuleName(path)
			--Robot.Debug(module_name)
			TreeCutter.Module = require(module_name)
			--Robot.Debug("Module Type: " .. tostring(type(TreeCutter.Module)))
			if TreeCutter.Module ~= nil then
				TreeCutter.ModuleFile = path
				if type(TreeCutter.Module) == type({}) then
					if TreeCutter.Module.TreeCutterVersion ~= nil then
						return true
					end
				end
				TreeCutter.UnloadModule()
			end
		end
	end
	return false
end

-- HELPER FUNCTION INTRODUCED AS OF VERSION 0.1
function TreeCutter.UnloadModule()
	if TreeCutter.ModuleFile == nil then return false end
	
	local module_name = TreeCutter.GetModuleName(TreeCutter.ModuleFile)
	
	--Robot.Debug("UnloadAPI: " .. module_name)
	
	local index_package = 0
	for k,m in pairs(package.loaded) do
		--Robot.Debug("k : " .. k)
		--Robot.Debug("m : " .. tostring(m))
		index_package = index_package + 1
		if k == module_name then
			package.loaded[module_name] = nil
			table.remove(package.loaded, index_package)
			--Robot.Debug("UnloadAPI: Package Removed")
			break
		end
	end
	local index_g = 0
	for k,m in pairs(_G) do
		--Robot.Debug("k : " .. k)
		--Robot.Debug("m : " .. tostring(m))
		--Robot.Debug("")
		index_g = index_g + 1
		if k == module_name then
			_G[module_name] = nil
			table.remove(_G, index_g)
			--Robot.Debug("UnloadAPI: _G Removed")
			break
		end
		--os.sleep(1)
	end
	
	TreeCutter.Module = nil
	TreeCutter.ModuleFile = nil

	--Robot.Debug("UnloadAPI: Finished")
	return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function TreeCutter.DetermineInventoryDropoff()
	return Robot.IsBelowChest()
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function TreeCutter.Initialize()
	if Robot.VERSION < TreeCutter.ROBOT_VERSION then
		TreeCutter.ErrorMessage = "Requires Minimum Robot API v" .. TreeCutter.ROBOT_VERSION
		return false
	end
	
	-- Sample From Potential Tree
	Robot.Message("SETUP> Get Tree Sample")
	local minecraft_id = Robot.Look(Robot.DIRECTION.FORWARD)
	
	-- LOAD TREECUTTER MODULE BASED ON TREE SAMPLE
	if fs.exists(TreeCutter.ModuleDirectory) then
		if fs.isDir(TreeCutter.ModuleDirectory) then
			Robot.Message("SETUP> Search for Module")
			for index,file in pairs(fs.list(TreeCutter.ModuleDirectory)) do
				if TreeCutter.LoadModule(TreeCutter.ModuleDirectory .. "/".. file) then
					
					if TreeCutter.Module.TreeCutterVersion <= TreeCutter.VERSION then
						if TreeCutter.Module.Initialize(Robot, TreeCutter, minecraft_id) then
							Robot.Message("SETUP> Found Module : " .. file)
							break;
						else
							--NOT THE CORRECT TREECUTTER MODULE
							TreeCutter.UnloadModule()
						end
					else
						TreeCutter.ErrorMessage = "SETUP> Module Requires v" .. TreeCutter.Module.TreeCuttterRequired
						TreeCutter.UnloadModule()
					end
					
				end
			end
		else
			TreeCutter.ErrorMessage = "SETUP> TreeType is not a directory"
		end
	else
		TreeCutter.ErrorMessage = "SETUP> TreeType directory does not exist"
	end
	
	return TreeCutter.Module ~= nil
end


-- MAIN FUNCTION INTRODUCED AS OF VERSION 0.1
function TreeCutter.Main()
	Robot.ClearScreen()
	Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
	Robot.Message("TreeCutter version "..TreeCutter.VERSION)
	Robot.Message("(c) Boxporium 2020")
	Robot.Message("Author(s): Neometron")
	Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
	
	if TreeCutter.Initialize() then
		local running = true
		while running do
			running = TreeCutter.Module.Update()
			if not TreeCutter.Module.Display() then
				Robot.Message("Default Display")
			end
		end
		TreeCutter.UnloadModule()
	end
	
	if TreeCutter.ErrorMessage ~= nil then
		Robot.Error(TreeCutter.ErrorMessage)
		return false
	else
		Robot.Message("TreeCutter FINISHED")
		return true
	end
end


-- RUN PROGRAM INTRODUCED AS OF VERSION 0.1
return TreeCutter.Main()