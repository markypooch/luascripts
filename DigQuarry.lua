local Robot = require "Robot"

local App = {}
App.NAME    = "DigQuarry"
App.VERSION = "v0.8"

local MAX_WIDTH  = 0
local MAX_LENGTH = 0
local MAX_DEPTH  = 0

local INVERT_DEPTH = false


function App.ReadNumber(str_message)
  if ((str_message == nil) or (str_message == ""))
  then
    str_message = App.NAME
  end
  
  local x, y = term.getCursorPos()
  
  local out_message = str_message.." > "
  local out_lenght  = string.len(out_message)
  
  print(out_message)
  
  term.setCursorPos( x + out_lenght, y )
  
  return tonumber(read())
end


function App.ReadString(str_message)
  if ((str_message == nil) or (str_message == ""))
  then
    str_message = App.NAME
  end
  
  local x, y = term.getCursorPos()
  local out_message = str_message.." > "
  local out_lenght  = string.len(out_message)
  
  print(out_message)
  
  term.setCursorPos( x + out_lenght, y )
  
  return read()
end


function App.PrintHR()
  print("______________________________")
  print()
end


function App.AnyKeyContinue()
  print()
  
  local out_message = "   < Press any key to continue >"
  
  print(out_message)
  local x, y = term.getCursorPos()
  
  os.pullEvent("key")
  
  term.setCursorPos( x, y - 1)
  term.clearLine()
  
  print()
end

function App.PrintHeader()
  term.clear()
  term.setCursorPos(1, 1)
  print(App.NAME.." "..App.VERSION)
  print("(c) Neometron 2018")
  App.PrintHR()
end


function App.Instructions()
  App.PrintHeader()
  print("Instructions:")
  print("Place and face turtle at the")
  print("start block of the quarry.")
  print("Note that turtle will make")
  print("the quarry moving to the right.")
  print("Enter the quarry imension.")
  print("Start and wait for completion.")
  
  print("Features not implemented yet:")
  print("1) Drop off inventory into chest.")
  print("2) Return to origin on completion.")
  
  App.PrintHR()
  App.AnyKeyContinue()
end


function App.Initialize()
 App.PrintHeader()
 print("Initialization:")
 print()
    
 MAX_WIDTH  = App.ReadNumber("Width")
 MAX_LENGTH = App.ReadNumber("Length")
 MAX_DEPTH  = App.ReadNumber("Depth")
    
 print("Initialization Complete")
 App.PrintHR()
 App.AnyKeyContinue()

end


function App.TurnLeft()
 local r = false
 r = Robot.TurnLeft()
 r = Robot.DigForward()
 r = Robot.MoveForward()
 r = Robot.TurnLeft()
 return r
end

function App.TurnRight()
 local r = false
 r = Robot.TurnRight()
 r = Robot.DigForward()
 r = Robot.MoveForward()
 r = Robot.TurnRight()
 return r
end


function App.Run()
 
 App.PrintHeader()
 print("Start Operation")
  
 local running = true
 local success = true

 local DIRECTION = {}
       DIRECTION.LEFT  = 0
       DIRECTION.RIGHT = 1
     
 local mirrored  = false     
 local direction = DIRECTION.RIGHT
 
 for d = 1, MAX_DEPTH, 1 do
  
  if MAX_WIDTH % 2 == 0 then
   mirrored = d % 2 == 0
  end
  
  if mirrored then
   direction = DIRECTION.LEFT
  else
   direction = DIRECTION.RIGHT
  end
  
  for w = 1, MAX_WIDTH, 1 do
   for l = 1, MAX_LENGTH, 1 do
    if l < MAX_LENGTH then
     success = Robot.DigForward()
     success = Robot.MoveForward()
    end
   end
   
   if w < MAX_WIDTH then
	if direction == DIRECTION.LEFT then
	 App.TurnLeft()
	 direction = DIRECTION.RIGHT
	elseif direction == DIRECTION.RIGHT then
	 App.TurnRight()
	 direction = DIRECTION.LEFT
	end
   end
   
  end


  if mirrored then
   success = Robot.TurnLeft()
   success = Robot.TurnLeft()
  else
   success = Robot.TurnRight()
   success = Robot.TurnRight()
  end
 
 
  if d < MAX_DEPTH then
   if INVERT_DEPTH then
   success = Robot.DigUp()
   success = Robot.MoveUp()
   else   
   success = Robot.DigDown()
   success = Robot.MoveDown()
   end
  end


 end  
 print("Finished Operation")
 App.PrintHR()
end


function App.Close()
  print(App.NAME.." Finished")
end



-- MAIN APP ENTRY --
local args = {...}
if #args == 3 then
  MAX_WIDTH  = tonumber(args[1])
  MAX_LENGTH = tonumber(args[2])
  MAX_DEPTH  = tonumber(args[3])
else
  App.Instructions()
  App.Initialize()
end

 if MAX_DEPTH < 0 then
  INVERT_DEPTH = true
  MAX_DEPTH = MAX_DEPTH * -1
 end

App.Run()
App.Close()