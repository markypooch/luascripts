local args = {...}
local Robot = {}

-- INTRODUCED 0.25
Robot.VERSION   = 0.28


-- INTRODUCED 0.25
-- ADDED      0.26: UP and DOWN
-- ADDED      0.27: FORWARD
Robot.DIRECTION =
{
  NORTH   = 0,
  EAST    = 1,
  SOUTH   = 2,
  WEST    = 3,
  UP      = 4,
  DOWN    = 5,
  FORWARD = 6
}


-- INTRODUCED 0.28
Robot.DIRECTION_NAME =
{
  NORTH   = "NORTH",
  EAST    = "EAST",
  SOUTH   = "SOUTH",
  WEST    = "WEST",
  UP      = "UP",
  DOWN    = "DOWN",
  FORWARD = "FORWARD"
}


-- INTRODUCED 0.28
function Robot.DirectionName(direction)

  if direction == Robot.DIRECTION.NORTH   then return Robot.DIRECTION_NAME.NORTH   end
  if direction == Robot.DIRECTION.EAST    then return Robot.DIRECTION_NAME.EAST    end
  if direction == Robot.DIRECTION.SOUTH   then return Robot.DIRECTION_NAME.SOUTH   end
  if direction == Robot.DIRECTION.WEST    then return Robot.DIRECTION_NAME.WEST    end
  if direction == Robot.DIRECTION.UP      then return Robot.DIRECTION_NAME.UP      end
  if direction == Robot.DIRECTION.DOWN    then return Robot.DIRECTION_NAME.DOWN    end
  if direction == Robot.DIRECTION.FORWARD then return Robot.DIRECTION_NAME.FORWARD end

  error("Invalid Robot.DIRECTION '"..direction.."'")
end


-- INTRODUCED 0.28
function Robot.DirectionEnum(direction_name)
  if direction_name ~= nil then 
    local d = direction_name:upper()
    if d == Robot.DIRECTION.NORTH   then return Robot.DIRECTION.NORTH   end
    if d == Robot.DIRECTION.EAST    then return Robot.DIRECTION.EAST    end
    if d == Robot.DIRECTION.SOUTH   then return Robot.DIRECTION.SOUTH   end
    if d == Robot.DIRECTION.WEST    then return Robot.DIRECTION.WEST    end
    if d == Robot.DIRECTION.UP      then return Robot.DIRECTION.UP      end
    if d == Robot.DIRECTION.DOWN    then return Robot.DIRECTION.DOWN    end
    if d == Robot.DIRECTION.FORWARD then return Robot.DIRECTION.FORWARD end
  end
  error("Invalid Robot.DIRECTION_NAME '"..d.."'")
end



-- INTRODUCED 0.25
Robot.direction = Robot.DIRECTION.NORTH

-- INTRODUCED 0.25
-- NOTE: Robot.DIRECTION.EAST  == +x
--       Robot.DIRECTION.UP    == +y
--       Robot.DIRECTION.NORTH == +z
Robot.origin    = vector.new(0, 0, 0)

-- INTRODUCED 0.25
-- NOTE: Robot.DIRECTION.EAST  == +x
--       Robot.DIRECTION.UP    == +y
--       Robot.DIRECTION.NORTH == +z
Robot.position  = vector.new(0, 0, 0)


-- INTRODUCED 0.25
function Robot.GetName()
  return os.getComputerLabel()
end


-- INTRODUCED 0.25
function Robot.SetName(name)
  return os.setComputerLabel(name)
end


-- INTRODUCED 0.25
function Robot.ClearScreen()
  term.clear()
  term.setCursorPos(1, 1)
end


-- INTRODUCED 0.25
function Robot.Message(message)
  local out_message = Robot.GetName().." : "..message
  local out_lenght  = string.len(out_message)
  print(out_message)
  return out_lenght
end


-- INTRODUCED 0.25
function Robot.Debug(message)  
  local out_message = Robot.GetName().." Debug : "..message
  local out_lenght  = string.len(out_message)
  print(out_message)
  return out_lenght
end


-- INTRODUCED 0.25
function Robot.Warning(message)
  local out_message = Robot.GetName().." Warning : "..message
  local out_lenght  = string.len(out_message)
  print(out_message)
  return out_lenght
end


-- INTRODUCED 0.25
function Robot.Error(message)
  local out_message = Robot.GetName().." Error : "..message
  local out_lenght  = string.len(out_message)
  print(out_message)
  return out_lenght
end


-- INTRODUCED 0.27
-- FUNCTION: Robot.RuntimeError(message)
-- DESC: Invoke a runtime error to terminate the program
-- PARAM1: (string) Error message
function Robot.RuntimeError(message)
  local out_message = Robot.GetName().." Error : "..message
  error(out_message)
end


-- INTRODUCED 0.25
function Robot.AnyKeyContinue()
  Robot.Message("PRESS ANY KEY TO CONTINUE")
  local x, y = term.getCursorPos()
  os.pullEvent("key")
  term.setCursorPos( x, y - 1)
  term.clearLine()
  print()
end


-- INTRODUCED 0.25
-- BUGFIX     0.28: Input cursor does not match same line if text scrolled
function Robot.InputString(str_message)
  if ((str_message == nil) or (str_message == ""))
  then
    str_message = ""
  end
  
  local x_max, y_max = term.getSize()
  local x, y = term.getCursorPos()
  
  local out_lenght  = Robot.Message(str_message.." > ")
  
  if y == y_max then
    term.setCursorPos( x + out_lenght, y - 1)
  else
    term.setCursorPos( x + out_lenght, y )
  end
  
  return read()
end


-- INTRODUCED 0.25
function Robot.InputNumber(str_message)  
  return tonumber(Robot.InputString(str_message))
end


-- INTRODUCED 0.25
function Robot.ReadLines(file)
  lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end


-- INTRODUCED 0.25
function Robot.StringSplit(inputstr, sep)
  if sep == nil then
    sep = "%s"
  end
  local t={} ; i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    t[i] = str
    i = i + 1
  end
  return t
end


-- INTRODUCED 0.27
-- FUNCTION: Robot.Look(direction)
-- DESC: The robot will determine what the adjacent block is by name.
-- PARAM1: A valid Robot.DIRECTION enum [FORWARD (default), UP, DOWN]
-- RETURN: (string) minecraft id
function Robot.Look(direction)
  local success, data = false, nil

  local direction = direction or Robot.DIRECTION.FORWARD

  if direction == Robot.DIRECTION.FORWARD then
    success, data = turtle.inspect()
  elseif direction == Robot.DIRECTION.UP then
    success, data = turtle.inspectUp()
  elseif direction == Robot.DIRECTION.DOWN then
    success, data = turtle.inspectDown()
  else
    Robot.RuntimeError("Robot.Look(direction): Invalid Param> " .. tostring(direction))
  end

  local minecraft_id = "minecraft:air"
  if success then
    minecraft_id = data.name
  end
  return minecraft_id
end


-- INTRODUCED 0.27
-- FUNCTION: Robot.IsDigHazardous(direction)
-- DESC: The robot will inspect the environment towards a specified direction
--       for any hazardous block type.
--       An example of a hazardous block is minecraft:gravel,
--       because of its ability to fall from gravity.
-- PARAM1: A valid Robot.DIRECTION enum [FORWARD (default), UP, DOWN]
-- RETURN: (boolean) true if hazardous block was found
function Robot.IsDigHazardous(direction)
  local direction = direction or Robot.DIRECTION.FORWARD
  if direction == Robot.DIRECTION.FORWARD
  or direction == Robot.DIRECTION.UP
  or direction == Robot.DIRECTION.DOWN
  then
    local minecraft_id = Robot.Look(direction)
      -- NOTE: UPDATE ROBOT API VERSION IF LIST IS ADDED TO
    if minecraft_id == "minecraft:sand"   --VERSION 0.27
    or minecraft_id == "minecraft:gravel" --VERSION 0.27
    then return true end
    return false
  else
    Robot.RuntimeError("Robot.IsDigHazardous(direction): Invalid Param> " .. tostring(direction))
  end
end


-- INTRODUCED 0.27
-- FUNCTION: Robot.IsDigPossible(direction)
-- DESC: The robot will inspect the environment towards a specified direction
--       and determine if block can be excavated using Robot.Dig(direction)
--       function. For instance, it is not possible to excavate minecraft:bedrock.
-- PARAM1: A valid Robot.DIRECTION enum [FORWARD (default), UP, DOWN]
-- RETURN: (boolean) true if digging is possible
function Robot.IsDigPossible(direction)
  local direction = direction or Robot.DIRECTION.FORWARD
  if direction == Robot.DIRECTION.FORWARD
  or direction == Robot.DIRECTION.UP
  or direction == Robot.DIRECTION.DOWN
  then
    local minecraft_id = Robot.Look(direction)
    -- NOTE: UPDATE ROBOT API VERSION IF LIST IS ADDED TO
    if minecraft_id == "minecraft:air"              --VERSION 0.27
    or minecraft_id == "minecraft:barrier"          --VERSION 0.27
    or minecraft_id == "minecraft:structure_void"   --VERSION 0.27
    or minecraft_id == "minecraft:structure_block"  --VERSION 0.27
    or minecraft_id == "minecraft:bedrock"          --VERSION 0.27
    or minecraft_id == "minecraft:end_portal_frame" --VERSION 0.27
    or minecraft_id == "minecraft:command_block"    --VERSION 0.27
    then return false end
    return true
  else
    Robot.RuntimeError("Robot.IsDigPossible(direction): Invalid Param> " .. tostring(direction))
  end
end


-- INTRODUCED 0.27
-- FUNCTION: Robot.Dig(direction)
-- DESC: The robot will attempt to dig in the specified direction.
-- PARAM1: A valid Robot.DIRECTION enum [FORWARD (default), UP, DOWN]
-- RETURN: (boolean) true if anything was dug
function Robot.Dig(direction)
  local dig_function = nil
  local dig_count = 0

  local  direction = direction or Robot.DIRECTION.FORWARD
  if     direction == Robot.DIRECTION.FORWARD then dig_function = turtle.dig
  elseif direction == Robot.DIRECTION.UP      then dig_function = turtle.digUp
  elseif direction == Robot.DIRECTION.DOWN    then dig_function = turtle.digDown
  else
    Robot.RuntimeError("Robot.Dig(direction): Invalid Param> " .. tostring(direction))
  end

  if Robot.IsDigPossible(direction) then
    -- Determine if the block in front of robot is kind that has gravity.
    -- The reason to know this is because there is a high chance that there
    -- is another block above it and it will fall once the robot digs forward.
    if Robot.IsDigHazardous(direction) then
      -- REPEAT DIGGING TILL HAZARD IS CLEARED
      local dig_running = true
      repeat 
        if dig_function() then dig_count = dig_count + 1 end
        dig_running = Robot.IsDigPossible(direction)
      until(dig_running == false)
    else 
      -- NORMAL DIGGING PROCEDURE
      if dig_function() then dig_count = dig_count + 1 end
    end
  end

  return dig_count > 0
end


-- INTRODUCED 0.25
-- VERSION 0.25 BUG FOUND: Function can return false even when block is excavated.
-- VERSION 0.27 DEPRECATED: Consider using Robot.Dig(direction) instead
function Robot.DigForward()

  local success, data = turtle.inspect()
  
  if success
  then
    if data.name == "minecraft:sand"
    or data.name == "minecraft:gravel"
    then
      while success 
    do
      success = turtle.dig()
    end
    success = true
  else
    success = turtle.dig()
    if success then
      success, data = turtle.inspect()
    if success then
      success = Robot.DigForward()
    end
    end
    end
  else
    success = true
  end
  
  return success
end


-- INTRODUCED 0.25
-- VERSION 0.27 DEPRECATED: Consider using Robot.Dig(direction) instead
function Robot.DigUp() 
  local success, data = turtle.inspectUp()
  
  if success
  then
    if data.name == "minecraft:sand"
    or data.name == "minecraft:gravel"
    then
      while success 
    do
      success = turtle.digUp()
    end
    success = true
  else
    success = turtle.digUp()
    if success then
      success, data = turtle.inspectUp()
    if success then
      success = Robot.DigUp()
    end
    end
    end
  else
    success = true
  end
  
  return success
end


-- INTRODUCED 0.25
-- VERSION 0.27 DEPRECATED: Consider using Robot.Dig(direction) instead
function Robot.DigDown()
  local success, data = turtle.inspectDown()
  
  if success
  then
    success = turtle.digDown()
  else
    success = true 
  end
  
  return success
end


-- INTRODUCED 0.25
function Robot.TillDown()
  --success, data = turtle.inspectDown()
  
  --if success then
  --  if data.name == "minecraft:dirt"
  --or data.name == "minecraft:grass"
  --then
  --  success = turtle.digDown()
  --  if not success then
  --    Robot.Debug("Wat-Da-Fuq")
  --  else
  --    success = true
  --  end
  --  --success = turtle.dig() -- This works
  --else
  --  success = false
  --end
  --else
  --  success = turtle.digDown()
  --if not success then
  --  Robot.Debug("Wat-Da-Fuq")
  --else
  --  Robot.Debug("Da-Fuq")
  --end
  ----success = turtle.dig() -- This works
  --end
  
  --if not success then
  --  success = turtle.digDown()
  --else
  --  success = false
  --end
  
  local success = turtle.digDown()
  
  return success
end


-- INTRODUCED 0.25
function Robot.RefuelWait()
  local success = false
  Robot.Message("INSERT FUEL [coal | lava]")
  while(turtle.getFuelLevel() == 0) 
  do
    for slot_index=1, 16, 1
    do
      turtle.select(slot_index)
      local slot_data = turtle.getItemDetail(slot_index)
      
      if slot_data then
        if slot_data.name == "minecraft:coal_block"
    or slot_data.name == "minecraft:coal"
    or slot_data.name == "minecraft:lava_bucket"
        then
          success = turtle.refuel()      
        end
      end
    
      os.sleep(1)
  end
  end
  
  success = turtle.getFuelLevel() > 0
  
  Robot.Message("RESUMING OPERATIONS")
  return success
end


-- INTRODUCED 0.25
function Robot.InventoryFullWait()
  error("Not Implemented")
end


-- INTRODUCED 0.25
function Robot.MoveUp()
  local success = turtle.up()
  
  if success
  then
    Robot.position = Robot.position + vector.new( 0, 1, 0)
  else
    if turtle.getFuelLevel() == 0 
  then
    Robot.Error("Out of fuel")
  else
    Robot.Error("Movement Obstructed")
  end
  end
  
  return success
end


-- INTRODUCED 0.25
function Robot.MoveUpEX()
  
  while not Robot.MoveUp() do
    if turtle.getFuelLevel() == 0 then
    Robot.RefuelWait()
  end
  os.sleep(3)
  end
  
  return true
end


-- INTRODUCED 0.25
function Robot.MoveDown()
  local success = turtle.down()
  
  if success
  then
    Robot.position = Robot.position + vector.new( 0,-1, 0)
  else
    if turtle.getFuelLevel() == 0 
  then
    Robot.Error("Out of fuel")
  else
    Robot.Error("Movement Obstructed")
  end
  end
  
  return success
end


-- INTRODUCED 0.25
function Robot.MoveDownEX()
  
  while not Robot.MoveDown() do
    if turtle.getFuelLevel() == 0 then
    Robot.RefuelWait()
  end
  os.sleep(3)
  end
  
  return true
end


-- INTRODUCED 0.25
function Robot.MoveForward()
  local success = turtle.forward()
  if success 
  then
    if     Robot.direction == Robot.DIRECTION.NORTH
    then   Robot.position  =  Robot.position + vector.new( 0, 0, 1)
    elseif Robot.direction == Robot.DIRECTION.EAST      
    then   Robot.position  =  Robot.position + vector.new( 1, 0, 0)
    elseif Robot.direction == Robot.DIRECTION.SOUTH      
    then   Robot.position  =  Robot.position + vector.new( 0, 0,-1)
    elseif Robot.direction == Robot.DIRECTION.WEST      
    then   Robot.position  =  Robot.position + vector.new(-1, 0, 0)
    else
    error("Invalid Robot.direction '"..direction.."'")
    end
  else
    if turtle.getFuelLevel() == 0 
  then
    Robot.Error("Out of fuel")
  else
    Robot.Error("Movement Obstructed")
  end
  end
  return success
end


-- INTRODUCED 0.25
function Robot.MoveForwardEX()

  while not Robot.MoveForward() do
    if turtle.getFuelLevel() == 0 then
    Robot.RefuelWait()
  end
  os.sleep(3)
  end

  return true
end


-- INTRODUCED 0.25
function Robot.MoveBackward()
  local success = turtle.back()
  if success 
  then
    if     Robot.direction == Robot.DIRECTION.NORTH
    then   Robot.position  =  Robot.position + vector.new( 0, 0,-1)
    elseif Robot.direction == Robot.DIRECTION.EAST      
    then   Robot.position  =  Robot.position + vector.new(-1, 0, 0)
    elseif Robot.direction == Robot.DIRECTION.SOUTH      
    then   Robot.position  =  Robot.position + vector.new( 0, 0, 1)
    elseif Robot.direction == Robot.DIRECTION.WEST      
    then   Robot.position  =  Robot.position + vector.new( 1, 0, 0)
    else
    error("Invalid Robot.direction '"..direction.."'")
    end
  else
    if turtle.getFuelLevel() == 0 
  then
    Robot.Error("Out of fuel")
  else
    Robot.Error("Movement Obstructed")
  end
  end
  return success
end


-- INTRODUCED 0.25
function Robot.MoveBackwardEX()
  
  while not Robot.MoveBackward() do
    if turtle.getFuelLevel() == 0 then
    Robot.RefuelWait()
  end
  os.sleep(3)
  end
  
  return true
end


-- INTRODUCED 0.25
function Robot.TurnLeft()
  local success = turtle.turnLeft()
  if success 
  then
    if     Robot.direction == Robot.DIRECTION.NORTH
    then   Robot.direction =  Robot.DIRECTION.WEST
    elseif Robot.direction == Robot.DIRECTION.EAST      
    then   Robot.direction =  Robot.DIRECTION.NORTH
    elseif Robot.direction == Robot.DIRECTION.SOUTH      
    then   Robot.direction =  Robot.DIRECTION.EAST
    elseif Robot.direction == Robot.DIRECTION.WEST      
    then   Robot.direction =  Robot.DIRECTION.SOUTH
    else
    error("Invalid Robot.direction '"..direction.."'")
    end
  end
  return success
end


-- INTRODUCED 0.25
function Robot.TurnRight()
  local success = turtle.turnRight()
  if success 
  then
    if     Robot.direction == Robot.DIRECTION.NORTH
    then   Robot.direction =  Robot.DIRECTION.EAST
    elseif Robot.direction == Robot.DIRECTION.EAST      
    then   Robot.direction =  Robot.DIRECTION.SOUTH
    elseif Robot.direction == Robot.DIRECTION.SOUTH      
    then   Robot.direction =  Robot.DIRECTION.WEST
    elseif Robot.direction == Robot.DIRECTION.WEST      
    then   Robot.direction =  Robot.DIRECTION.NORTH
    else
    error("Invalid Robot.direction '"..direction.."'")
    end
  end
return success
end


-- INTRODUCED 0.25
function Robot.InventorySelect(slot_id)
  return turtle.select(slot_id)
end


-- INTRODUCED 0.28
function Robot.InventorySlot()
  return turtle.getSelectedSlot()
end


-- INTRODUCED 0.25
function Robot.InventoryName()
  local item = turtle.getItemDetail()
  
  if item then
    --REFERENCE
    --print("Item name: ", item.name)
    --print("Item damage value: ", item.damage)
    --print("Item count: ", item.count)
    return item.name
  end
  
  return false
end


-- INTRODUCED 0.25
function Robot.InventoryDamage()
  local item = turtle.getItemDetail()
  
  if item then
    --REFERENCE
    --print("Item name: ", item.name)
    --print("Item damage value: ", item.damage)
    --print("Item count: ", item.count)
    return item.damage
  end
  
  return false
end


-- INTRODUCED 0.25
-- UPDATE 0.28: return 0 instead of false
function Robot.InventoryCount()
  local item = turtle.getItemDetail()
  
  if item then
    --REFERENCE
    --print("Item name: ", item.name)
    --print("Item damage value: ", item.damage)
    --print("Item count: ", item.count)
    return item.count
  end
  
  return 0
end


-- INTRODUCED 0.25
function Robot.InventoryValidate()
  local item = nil
  
  item = turtle.getItemDetail(1)
  if item 
  then
    --REFERENCE
    --print("Item name: ", item.name)
    --print("Item damage value: ", item.damage)
    --print("Item count: ", item.count)
    
    if item.name == "minecraft:bucket" 
    or item.name == "minecraft:water_bucket"
    then
    if item.count == 1
    then
      return true
    elseif item.count > 1
    then
      print("Error: Too many buckets")
      return false
    end
    else
    print("Error: Slot 1 should only be a bucket.")
    return false
    end
  else
    print("Error: Slot 1 missing a bucket.")
    return false
  end--if item then

--item = turtle.getItemDetail(2)
--if item then
--end

end--InventoryValidate()


-- INTRODUCED 0.25
function Robot.InventoryFind(item_name, start_slot, end_slot)
  for index = start_slot, end_slot, 1
  do
    local item = turtle.getItemDetail(index)
  if item then
    if item.name == item_name then
      return true, index
    end
  end
  end
  return false, 0
end


-- INTRODUCED 0.25
function Robot.PlaceForward()
  local success = turtle.place()
  return success
end


-- INTRODUCED 0.25
function Robot.PlaceDown()
  local success = turtle.placeDown()
  return success
end


-- INTRODUCED 0.25
function Robot.PlaceUp()
  local success = turtle.placeUp()
  return success
end


-- INTRODUCED 0.25
function Robot.PlantDown()
  return Robot.PlaceDown()
end


-- INTRODUCED 0.25
function Robot.FillBucket()
  if  Robot.InventoryValidate() 
  and Robot.InventorySelect(1) 
  then
    local item = turtle.getItemDetail(1)
    if item 
    then
    if item.name == "minecraft:bucket" 
    then
      if turtle.placeDown() 
      then
      return true
      else
      print("Error: Failed to fill bucket.")
      return false
      end
    else
      return true --Already have a filled bucket
    end
    else
    print("Error: Slot 1 missing a bucket.")
    return false
    end--if item then
  else
    print("Error: Invalid Inventory.")
    return false
  end
end--FillBucket()


-- INTRODUCED 0.25
function Robot.EmptyBucket()
  if  Robot.InventoryValidate() 
  and Robot.InventorySelect(1) 
  then
    local item = turtle.getItemDetail(1)
    if item 
    then
    if item.name == "minecraft:water_bucket" 
    then
      if turtle.placeDown() 
      then
      return true
      else
      print("Error: Failed to empty bucket.")
      return false
      end
    else
      return true --Already have an empty bucket
    end
    else
    print("Error: Slot 1 missing a bucket.")
    return false
    end--if item then
  else
    print("Error: Invalid Inventory.")
    return false
  end
end--FillBucket()


-- INTRODUCED 0.26
-- ADDED Robot.CheckInventoryFull()
function Robot.CheckInventoryFull()
  local slots_used = 0
  
  for i = 1, 16 do
    if ( turtle.getItemCount(i) > 0 ) then
      slots_used = slots_used + 1
    end
  end
  
  if ( slots_used == 16 ) then
    --Robot.Debug("Inv slots used: " .. tostring(slots_used))
    return true
  end
  
  return false
end


-- INTRODUCED 0.26
-- ADDED Robot.IsInventoryEmpty()
function Robot.IsInventoryEmpty()
  local is_empty = true
  for i = 1, 16 do
    if ( turtle.getItemCount(i) > 0 ) then
      is_empty = false
    end    
  end
  return is_empty
end


-- INTRODUCED 0.26
-- DEPRECATED 0.28: Use Robot.IsChest(direction)
function Robot.IsBelowChest()
  return Robot.IsChest(Robot.DIRECTION.DOWN)
end


-- INTRODUCED 0.28
function Robot.IsChest(direction)
  local is_valid = false
  local data     = nil
  
  if direction == Robot.DIRECTION.FORWARD then
    is_valid, data = turtle.inspect()
  elseif direction == Robot.DIRECTION.UP then
    is_valid, data = turtle.inspectUp()
  elseif direction == Robot.DIRECTION.DOWN then
    is_valid, data = turtle.inspectDown()
  else
    is_valid = false
  end

  if is_valid and data ~= nil then
    for key, value in pairs(data) do
      if ( key == "name" ) then
        if ( value == "minecraft:chest" ) then
          return true
        end
      end
    end  
  end
  
  return false
end


-- FUNCTION: Robot.UnloadInventory(direction)
-- DESC    : Unloads the active inventory slot into a valid chest or
--           waits for user interaction to manually unload inventory.
-- PARAM 1 : Enumerator : Default = Robot.DIRECTION.DOWN
-- RETURN  : Boolean    : Success == true
--
-- INTRODUCED 0.26: direction not implemented; Default Robot.DIRECTION.DOWN
-- DEPRECATED 0.28: Use Robot.PushInventory(direction, count)  
function Robot.UnloadInventory(direction)
  return Robot.PushInventory(direction, 64, false)
end



-- FUNCTION: Robot.PushInventory(direction, count)
-- DESC    : Unloads the active inventory slot into a valid chest or
--           waits for user interaction to manually unload inventory.
-- PARAM 1 : Enumerator : Default = Robot.DIRECTION.DOWN
-- PARAM 2 : Integer    : Default = 64
-- PARAM 3 : Boolean    : Default = true
-- RETURN  : Boolean    : Success Code
--
-- INTRODUCED 0.28: PARAM 3 not implemented yet
function Robot.PushInventory(direction, count, user_interaction)  
  if direction == nil then direction = Robot.DIRECTION.DOWN end
  if count     == nil then count     = 64                   end
  if user_interaction == nil then user_interaction = true   end
  
  if Robot.InventoryCount() > 0 then
    if Robot.IsChest(direction) then
      
	  -- Get drop function
      local  drop = nil

	  if     direction == Robot.DIRECTION.FORWARD then drop = turtle.drop
      elseif direction == Robot.DIRECTION.UP      then drop = turtle.dropUp
      elseif direction == Robot.DIRECTION.DOWN    then drop = turtle.dropDown
      else error("Invalid Direction")
      end
      
	  -- Drop inventory
      while Robot.InventoryCount() > 0 and not drop(count) do
	    Robot.Error("Chest Full: "..Robot.DirectionName(direction))
		Robot.AnyKeyContinue()
	  end
	  
      return true
    
	else 
	
	  Robot.Error("Chest Missing: "..Robot.DirectionName(direction))
	  
	  while Robot.InventoryCount() > 0 do
        Robot.Message("USER> Unload Slot "..tostring(Robot.InventorySlot()))
        Robot.AnyKeyContinue()
	  end
	  
	  return true
    end
  end
  
  return false
end



-- FUNCTION: Robot.PullInventory(direction, count, user_interaction)
-- DESC    : Pulls inventory from a valid chest into the active inventory slot.
-- PARAM 1 : Enumerator : Default = Robot.DIRECTION.DOWN
-- PARAM 2 : Integer    : Default = 64
-- PARAM 3 : Boolean    : Default = true
-- RETURN  : Boolean    : Success Code
--
-- INTRODUCED 0.28
function Robot.PullInventory(direction, count, user_interaction)
  if direction == nil then direction = Robot.DIRECTION.DOWN end
  if count     == nil then count     = 64                   end
  if user_interaction == nil then user_interaction = true   end
  
  if Robot.IsChest(direction) then
    -- Get suck function
    local  suck = nil
    if     direction == Robot.DIRECTION.FORWARD then suck = turtle.suck
    elseif direction == Robot.DIRECTION.UP      then suck = turtle.suckUp
    elseif direction == Robot.DIRECTION.DOWN    then suck = turtle.suckDown
    else error("Invalid Direction")
    end
    -- Suck inventory
	if suck(count) 
	  then return true
	  else
	    if user_interaction 
		  then
		    Robot.Error("Chest Empty: "..Robot.DirectionName(direction))
			Robot.Message("USER> Fill Chest: "..Robot.DirectionName(direction))
		    Robot.AnyKeyContinue()
			-- RECURSIVE RETRY
			return Robot.PullInventory(direction, count, user_interaction)
		  else
		    Robot.Warning("Chest Empty: "..Robot.DirectionName(direction))
		end
	end
  else
    -- MISSING CHEST
	if user_interaction then
	  Robot.Error("Chest Missing: "..Robot.DirectionName(direction))
	  Robot.Message("USER> Install Chest: "..Robot.DirectionName(direction))
	  Robot.AnyKeyContinue()
	  -- RECURSIVE RETRY
	  return Robot.PullInventory(direction, count, user_interaction)
	else
	  Robot.Warning("Chest Missing: "..Robot.DirectionName(direction))
	end  
  end
  -- FAILED TO PULL INVENTORY FROM CHEST
  return false
end



-- FUNCTION: Robot.DumpInventory(direction)
-- DESC    : Unloads all inventory slots into a valid chest or
--           waits for user interaction to manually unload inventory.
-- PARAM 1 : direction : Enumerator : Default = Robot.DIRECTION.DOWN
-- RETURN  : success   : Boolean
--
-- INTRODUCED 0.28
function Robot.DumpInventory(direction)
  for slot_id = 1, 16 do
    if Robot.InventorySelect(slot_id) then
	  Robot.PushInventory(direction)
	end
  end
end



-- RETURN ROBOT API
return Robot