function string:split( inSplitPattern, outResults )
    if not outResults then
        outResults = { }
    end
    local theStart = 1
    local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
    while theSplitStart do
        table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
        theStart = theSplitEnd + 1
        theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
    end
    table.insert( outResults, string.sub( self, theStart ) )
    return outResults
end


function restful_request(url, method, body, content_type, token)
    headers = {}
    headers['Content-Type']  =content_type
    headers['content-encoding'] = "identity"
    
    response = nil
    err      = nil
    
    if (method == "GET") then
        response, err = http.get(url, headers)
    elseif (method == "PUT") then
        response, err = http.put(url, headers) 
    else
        response, err = http.post(url, body, headers)
    end
    
    filed = io.open("dump.txt", "w")
    filed:close()
    
    if (err ~= nil) then
        print("Error executing restful request")
        print(err)
        return nil
    end
    
    return response
end    

function main()
    print("Distributed Dechunkifier")
    print("-------------------------------")
    
    print("How many clients?")
    local clients = io.read()
    
    list  = {}
    count = 0
    input = ""
    print("Type 'Quit' to quit")
    
    while input ~= "Quit" do
        print("ItemID: ")
        input = io.read()
        if input ~= "Quit" then
            count = count + 1
            list[count] = input
        end
    end
    
    for i=1, count do
        print(list[i])
    end
        
      
    print("Press any key to start...")
       
    body = '{"num_of_clients":' .. clients .. ','
    filterList = '"filter_list":['
    for i=1, count do
        if i < count then
            filterList = filterList .. '"' .. list[i] .. '"' .. ','
        else
            filterList = filterList .. '"' .. list[i] .. '"' .. ']}'
        end
    end
    
    print(filterList)
    
    body = body .. filterList .. ''
    print(body)    
    response = restful_request('https://qsbx2bcx1m.execute-api.us-east-1.amazonaws.com/prod/', 'POST', body, "application/json", "")          
    
    while 1 do
        term.clear()
        print("Sever console")
        print("-------------------")
        print("Enter ID of turtle:")
        print("Or Type, 'Go' to reissue start cmd")
        cmd = io.read()
        
        if cmd == "Go" then
            response = restful_request('https://qsbx2bcx1m.execute-api.us-east-1.amazonaws.com/prod/', 'POST', body, "application/json", "")
        else
            response = restful_request('https://qsbx2bcx1m.execute-api.us-east-1.amazonaws.com/prod/report?client_id=' .. cmd .. '', 'GET', body, "application/json", "")
            --print(response)
            --print(response.getResponseStatusCode())
            if response ~= nil then
                bodystatus = response.readAll()
                if bodystatus ~= "No Value Retrieved" then
                    report = bodystatus:split(",")
                    items = report[3]:split(".")

                    term.clear()
                    print("Reporting on Turtle: " .. cmd .. "")
                    print("----------------------------------")
                    print("Status: " .. report[1])
                    print("Fuel Level: " .. report[2])
                end

                os.sleep(5)
            end                   
        end          
    end    
end

main()
