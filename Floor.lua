local slot = 1

turtle.select(slot)
turtle.placeDown()

while turtle.forward() do
 turtle.select(slot)
 if not turtle.placeDown() then
  slot = slot + 1
  if slot > 16 then slot = 1 end
 end
end
