function equip(name)
    for i=1, 16 do
        turtle.select(i)
        local data = turtle.getItemDetail(i)
        if (data ~= nil) then
            print(data.name)
            if (data.name == name) then
                turtle.equipRight()
                break
            end
        end
    end
end

function locateTargetPlayer(lidar, name)
    ret = {}
    local players = {lidar.scanPlayersPos()}
    for i=1, #players, 6 do
        if players[i] == name then
            ret[1] = players[i+3]
            ret[2] = players[i+4]
            ret[3] = players[i+5]
            
            ret[4] = players[i+1]
            ret[5] = players[i+2]
            
            return ret
        end
    end
end

function main()

    local lidar = peripheral.wrap('left')
    
    local positionX = 0
    local positionY = 0
    local positionZ = 0
    
    local angle = 0
    local turn = ""
    
    while true do
    
    index = 1
    while true do
    
        print("Searching...")
        
        local ret = locateTargetPlayer(lidar, "markypooch")
        
        if #ret > 0 then
            positionX = ret[1]
            positionY = ret[2]
            positionZ = ret[3]
            
            print(positionX)
            
            angle = ret[4]
            turn = ret[5]
            break
        end
    end
            
    local turtlePos = {lidar.getTurtlePos()}
    
    local dX = math.abs(positionX - turtlePos[1])
    local dY = positionY - turtlePos[2]
    local dZ = math.abs(positionZ - turtlePos[3])
    
    if (angle > 60) then
        if (turn == "left") then
            turtle.turnLeft()
        elseif (turn == "right") then
            turtle.turnRight()
        end
    end
    
    if (dX > 0.5 or dZ > 0.5) then
        if (not turtle.forward() and dY == 0) then
            print(positionX)
            print(positionZ)
            
            local iPosX = math.floor(positionX)
            local iPosZ = math.floor(positionZ)
            print(iPosX)
            print(iPosZ)
            local path = {lidar.Pathfind(iPosX, iPosZ, "block.minecraft.bamboo,block.minecraft.sugar_cane,door,glass")}
            local pathLength = #path
            print("path length: "..pathLength.."")
            pfIndex = 1
            while true do
                print("pathfinding")
                
                --local r = locateTargetPlayer(lidar, "markypooch")
                
                --if (r[1] ~= positionX or r[2] ~= positionY or r[3] ~= positionZ) then
                --    break
                --end
                                           
                pfX = path[pfIndex]
                pfY = path[pfIndex+1]
                pfZ = path[pfIndex+2]
                
            
                
                if (pfX == nil or pfY == nil or pfZ == nil) then
                    print("Entry was nil")
                    break
                end
                
                --print(pfX)
                --print(pfZ)
                
                
                --if (pfY ~= positionY) then
                --    break
                --end
                         
                local r = {lidar.getAngleAndHemisphereOfPosition(pfX, pfY, pfZ)}
                
                print(r[1])
                --print(r[2])
                
                if (r[1] == nil) then
                    print("No angle")
                end
                
                timesToTurn = math.floor(r[1] / 90)
                print(timesToTurn)
                for i=1, timesToTurn do
                    if (r[1] > 60) then
                        if (r[2] == "left") then
                            turtle.turnLeft()
                        else
                            turtle.turnRight()
                        end
                    end            
                end
                
                if (not turtle.forward()) then
                    print("Fail")
                    equip("minecraft:diamond_pickaxe")
                    turtle.dig()
                    equip("minecraft:diamond_sword")                    
                end
                
                turtle.attack()
                
                if (pfIndex+3 >= pathLength) then
                    break
                else
                    pfIndex = pfIndex + 3
                end
            end    
            
        end
    end
    
    if (dY > 0.5) then
        turtle.up()
    end
    
    if (dY < -0.5) then
        turtle.down()
    end
    
    
    os.sleep(0.05)
    
    end
end

main()