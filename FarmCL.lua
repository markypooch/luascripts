local args = {...}
local App = { NAME = "FarmCL", VERSION = 0.2, ROBOT_VERSION = 0.27, Farm = {} }
local Robot = nil


-- INTRODUCED: v0.1
function App.Main()

  Robot.ClearScreen()
  Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
  Robot.Message(App.NAME.." version "..App.VERSION)
  Robot.Message("(c) Boxporium 2021")
  Robot.Message("Author(s): Neometron")
  Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
  
  if not App.Initialize() then 
    return false 
  end
  
  if not App.ArgSetup() then
	if not App.UserSetup() then
	  Robot.Message(App.NAME.." Finished")
	  return false 
	end
  end
  
  Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
  
  local chunkloader = nil
  if  peripheral.isPresent('left') 
  and peripheral.getType('left') == "chunkloader" 
  then
    chunkloader = peripheral.wrap('left')
	chunkloader.setEnable(true)
  elseif peripheral.isPresent('right') 
  and peripheral.getType('right') == "chunkloader"
  then
    chunkloader = peripheral.wrap('right')
	chunkloader.setEnable(true)
  end
  
  --TODO Make loop have run variable
  while true do
	-- PHASE 1 --
    App.Farm.InventoryReset()
	App.Farm.GotoWork()
	App.Farm.Planting()
	App.Farm.GotoOrigin()
	-- PHASE 2 --
	
	App.Farm.Waiting()
	
	-- PHASE 3 --
	App.Farm.InventoryReset()
	App.Farm.GotoWork()
	App.Farm.Harvesting()
	App.Farm.GotoOrigin()
  end
  
  if chunkloader ~= nil then
    chunkloader.setEnable(false)
  end
  
  Robot.Message(App.NAME.." Finished")
  return true
end


-- INTRODUCED: v0.1
function App.Initialize()

  App.Farm.WIDTH            = 0
  App.Farm.LENGTH           = 0
  App.Farm.SEED             = "minecraft:air"
  App.Farm.CROP             = "minecraft:air"
  App.Farm.CHEST_IN         = false
  App.Farm.CHEST_OUT        = false
  App.Farm.WAIT_PERIOD      = 0
  App.Farm.RESUME_WIDTH     = 1
  App.Farm.RESUME_LENGTH    = 1
  App.Farm.RESUME_DIRECTION = Robot.DIRECTION.NORTH
  
  --App.offset = vector.new( 0, 0, 0)
  --App.offset_direction = Robot.DIRECTION.NORTH

  return true
end



-- INTRODUCED: v0.1
function App.Close()
  print(App.NAME.." Finished")
end



-- INTRODUCED: v0.1
function App.ArgSetup()
  --TODO Process program arguments
  return false
end



-- INTRODUCED: v0.1
function App.UserSetup()

  Robot.Message("Farm Initialization:")

  -- GET FIELD LENGTH --
  while App.Farm.LENGTH == 0 do
    App.Farm.LENGTH = Robot.InputNumber("Field Length");
    if App.Farm.LENGTH == nil or App.Farm.LENGTH < 1 then
      Robot.Error("Invalid: Length < 1")
      App.Farm.LENGTH = 0
    end
  end
  
  -- GET FIELD WIDTH --
  while App.Farm.WIDTH == 0 do
    App.Farm.WIDTH = Robot.InputNumber("Field Width");
    if App.Farm.WIDTH == nil or App.Farm.WIDTH < 1 then
      Robot.Error("Invalid: Width < 1")
      App.Farm.WIDTH = 0
    end
  end
  
  -- GET FIELD WIDTH --
  while App.Farm.WAIT_PERIOD == 0 do
    App.Farm.WAIT_PERIOD = Robot.InputNumber("Wait Period (minutes)");
    if App.Farm.WAIT_PERIOD == nil or App.Farm.WAIT_PERIOD < 1 then
      Robot.Error("Invalid: Width < 1")
      App.Farm.WAIT_PERIOD = 0
    end
  end
  
  -- CHECK VALID INPUT CHEST AND GET SEED TYPE --
  Robot.Message("Checking Input Chest (RIGHT)")
  Robot.TurnRight()
  while not App.Farm.CHEST_IN do
	App.Farm.CHEST_IN = Robot.IsChest(Robot.DIRECTION.FORWARD)
	if not App.Farm.CHEST_IN then
	  Robot.Error("USER> place chest in front")
	  Robot.AnyKeyContinue()
	else
	  Robot.Message("Registering Seed Type")
	  if  Robot.InventorySelect(1) 
	  and Robot.PullInventory(Robot.DIRECTION.FORWARD, 1, false) then
	    local item_name = Robot.InventoryName()
		
		while not App.Farm.ValidateSeed(item_name) do
		  Robot.Error("Invalid Seed: "..item_name)
		  Robot.Message("USER> Fix Input Inventory")
		  Robot.AnyKeyContinue()
		  item_name = Robot.InventoryName()
		end
		
		App.Farm.SEED = item_name
		Robot.Message("Registered: "..App.Farm.SEED)
		App.Farm.CHEST_IN = true
		turtle.drop(1)
	  else
	    Robot.Error("USER> place seed in input chest")
	    Robot.AnyKeyContinue()
	  end
	end
  end
  Robot.TurnLeft()
  
  -- CHECK VALID OUTPUT CHEST --
  Robot.Message("Checking Output Chest (DOWN)")
  while not App.Farm.CHEST_OUT do
    App.Farm.CHEST_OUT = Robot.IsChest(Robot.DIRECTION.DOWN)
	if not App.Farm.CHEST_OUT then
	  Robot.Error("USER> place chest below")
	  Robot.AnyKeyContinue()
	end
  end
  
  Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
  Robot.Message("Farm Settings")
  Robot.Message("Width : "..tostring(App.Farm.WIDTH))
  Robot.Message("Length: "..tostring(App.Farm.LENGTH))
  Robot.Message("Seed  : "..App.Farm.SEED)
  Robot.Message("Period: "..App.Farm.WAIT_PERIOD)
  Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
  
  local is_correct = Robot.InputString("Correct? (yes|no)")
  if is_correct == nil then return false end
  is_correct = is_correct:lower()
  if is_correct == "yes" or is_correct == "y" then return true end
  if App.Initialize() then return App.UserSetup() end
 
  return false
end



-- INTRODUCED: v0.1
function App.Farm.PullInput(slot, count)

  if Robot.InventorySelect(slot) then
    return Robot.PullInventory(Robot.DIRECTION.FORWARD, count)
  end
  
  return false
end



-- INTRODUCED: v0.1
function App.Farm.PushInput(slot, count)
  
  if Robot.InventorySelect(slot) and Robot.InventoryCount() > 0 then
    if Robot.InventoryName() == App.Farm.SEED then
	  return Robot.PushInventory(Robot.DIRECTION.FORWARD, count)
	else 
	  Robot.Warning("Skip Slot "..tonumber(slot).." : "..Robot.InventoryName())
	end
  end
      
  return false
end



-- INTRODUCED: v0.1
function App.Farm.PushOutput(slot, count)
  
  if Robot.InventorySelect(slot) and Robot.InventoryCount() > 0 then
    if Robot.InventoryName() ~= App.Farm.SEED then
	  return Robot.PushInventory(Robot.DIRECTION.DOWN, count)
	else 
	  Robot.Warning("Skip Slot "..tonumber(slot).." : "..Robot.InventoryName())
	end
  end
 
  return false
end



-- INTRODUCED: v0.1
function App.Farm.InventoryReset()
  Robot.Message("Resetting Inventory")
  
  Robot.Message("Output Yield")
  for slot = 1, 16, 1 do
	App.Farm.PushOutput(slot, 64)
  end
  
  Robot.TurnRight()
  
  Robot.Message("Output Seed")
  for slot = 1, 16, 1 do
	App.Farm.PushInput(slot, 64)
  end
  
  local required_seed_count = App.Farm.WIDTH * App.Farm.LENGTH
  
  Robot.Message("Input Seed")
  App.Farm.PullInput(1, 64)
  
  
  Robot.TurnLeft()
  
  Robot.Message("Inventory Ready")
end



-- INTRODUCED: v0.1
function App.Farm.GotoOrigin(set_resume)

  Robot.Message("Returning to Origin")
  
  if set_resume == nil then set_resume = false end
  
  local WIDTH_MAX  = Robot.position.x
  local LENGHT_MAX = Robot.position.z
  
  if set_resume then
    App.Farm.RESUME_WIDTH     = WIDTH_MAX
    App.Farm.RESUME_LENGTH    = LENGHT_MAX
    App.Farm.RESUME_DIRECTION = Robot.direction
  else
    App.Farm.RESUME_WIDTH  = 1
    App.Farm.RESUME_LENGTH = 1
    App.Farm.RESUME_DIRECTION = Robot.DIRECTION.NORTH
  end
  
  if Robot.direction == Robot.DIRECTION.NORTH then
    success = Robot.TurnLeft()
	success = Robot.TurnLeft()
    for l = 1, LENGHT_MAX do
	  if l < LENGHT_MAX then
	    success = Robot.MoveForwardEX()
	  end
    end
  end
  
  success = Robot.TurnRight()
  
  for w = 1, WIDTH_MAX do
    if w < WIDTH_MAX then
      success = Robot.MoveForwardEX()
	end
  end
  
  success = Robot.TurnLeft()
  success = Robot.MoveForwardEX()
  success = Robot.TurnLeft()
  success = Robot.TurnLeft()
  
  Robot.Message("At Origin")
  
end



-- INTRODUCED: v0.1
function App.Farm.GotoWork()
  Robot.Message("Returning to Work")
  
  -- MOVE ONTO FIELD --
  success = Robot.MoveForwardEX()
  
  -- Robot.lua HACK: Reset Robot Position
  Robot.position = vector.new(1, 0, 1)
  --Robot.Debug(Robot.position.tostring(Robot.position))
  
  -- RESUME LENGTH POSITION
  
  for l = 1, App.Farm.RESUME_LENGTH do
    --Robot.Debug("lenght == "..tostring(l))
    --Robot.AnyKeyContinue()
	if l < App.Farm.RESUME_LENGTH then
      success = Robot.MoveForwardEX()
	end
  end
  
  -- RESUME WIDTH POSITION
  if App.Farm.RESUME_WIDTH > 1 then success = Robot.TurnRight() end
  for w = 1, App.Farm.RESUME_WIDTH do
    --Robot.Debug("width == "..tostring(w))
    if w < App.Farm.RESUME_WIDTH then
      success = Robot.MoveForwardEX()
    end
  end
  

  -- RESTORE PREVIOUS ROBOT ORIENTATION
  if     Robot.direction == Robot.DIRECTION.EAST 
  and    App.Farm.RESUME_DIRECTION == Robot.DIRECTION.NORTH
  then
    -- TURN LEFT    
    success = Robot.TurnLeft()
  elseif Robot.direction == Robot.DIRECTION.EAST 
  and    App.Farm.RESUME_DIRECTION == Robot.DIRECTION.SOUTH
  then
    -- TURN RIGHT   
    success = Robot.TurnRight()
  elseif Robot.direction == Robot.DIRECTION.NORTH
  and    App.Farm.RESUME_DIRECTION == Robot.DIRECTION.SOUTH
  then  
    -- TURN AROUND  
    success = Robot.TurnRight()
	success = Robot.TurnRight()
  end
  
  Robot.Message("Starting Work")
end



-- INTRODUCED: v0.1
function App.Farm.Planting()
  Robot.Message("Planting")
  
  for w = 1, App.Farm.WIDTH do
	for l = 1, App.Farm.LENGTH do

	  -- GET SEED --
      success, slot_id = Robot.InventoryFind(App.Farm.SEED, 1, 16)
	  
	  -- REFILL MISSING SEED --
	  while not success do
		Robot.Message("Awaiting Refill : "..App.Farm.SEED)
	    success, slot_id = Robot.InventoryFind(App.Farm.SEED, 1, 16)
		if success then
		  Robot.Message("Resuming Operation")
		else
		  Robot.AnyKeyContinue()
		end
	  end
	  
	  -- TILL & PLANT SEED --
	  if Robot.InventorySelect(slot_id) then
	    --Robot.Debug("TILL "..tonumber(l));
		--Robot.AnyKeyContinue()
		if Robot.TillDown() then
		  -- WHILE WE ARE HERE CLEANUP THE FIELD --
		  App.Farm.Vacuum()
		else
		  Robot.Warning("Till Failed : "..tostring(w)..","..tostring(l))
	    end
		
		--Robot.AnyKeyContinue()
		if not Robot.PlantDown() then 
		    Robot.Warning("Plant Failed : "..App.Farm.SEED)
			Robot.Warning("Plant Failed : "..tostring(w)..","..tostring(l))  
	    end
	  else
		error("Inventory Select : "..tostring(slot_id))
	  end
	  
	  -- MOVE ROBOT DOWN THE LINE --
	  --Robot.Debug("lenght == "..tostring(l));
	  if l < App.Farm.LENGTH then
        success = Robot.MoveForwardEX() 
	  end
	  --sleep(1)
    end
    
	-- TURN THE ROBOT FOR NEXT LINE --
	--Robot.Debug("width == "..tostring(w));
	if w < App.Farm.WIDTH then
	  --sleep(1)
	  if w % 2 == 0 then
        success = Robot.TurnLeft()
        success = Robot.MoveForwardEX()
        success = Robot.TurnLeft()
      else
        success = Robot.TurnRight()
        success = Robot.MoveForwardEX()
        success = Robot.TurnRight()
      end
	end
	
  end
  
  Robot.Message("Planting Done")
end



-- INTRODUCED: v0.1
function App.Farm.Harvesting()
  Robot.Message("Harvesting")
  
  for w = 1, App.Farm.WIDTH, 1 do
	for l = 1, App.Farm.LENGTH, 1 do
	  -- WHILE WE ARE HERE CLEANUP THE FIELD --
      App.Farm.Vacuum()
	  
	  -- MOVE ROBOT DOWN THE LINE --
	  if l < App.Farm.LENGTH then
        success = Robot.MoveForwardEX() 
	  end
    end
    
	-- TURN THE ROBOT FOR NEXT LINE --
	if w < App.Farm.WIDTH then
	  if w % 2 == 0 then
        success = Robot.TurnLeft()
        success = Robot.MoveForwardEX()
        success = Robot.TurnLeft()
      else
        success = Robot.TurnRight()
        success = Robot.MoveForwardEX()
        success = Robot.TurnRight()
      end
	end
	
  end
    
  Robot.Message("Harvesting Done")
end



-- INTRODUCED: v0.1
function App.Farm.Vacuum()
  return turtle.suckDown()
end



-- INTRODUCED: v0.1
-- UPDATE: v0.2 - Make the robot walk the field 
--                to get chunk loader coverage. 
function App.Farm.Waiting()
  Robot.Message("Field Growing")
  
  local countdown = App.Farm.WAIT_PERIOD * 60
  local tx, ty = term.getCursorPos()
  ty = ty - 1
  
  while countdown > 0 do
    App.Farm.GotoWork()
	
	term.setCursorPos(1, ty)
	term.clearLine()
	term.setCursorPos(1, ty)
    Robot.Message("Waiting ["..tostring(countdown).."]")
	
	-- WALK EVERY 5 MIN
	if countdown % 300 == 0 then
	
	  for w = 1, App.Farm.WIDTH, 1 do
	    for l = 1, App.Farm.LENGTH, 1 do
	  
          term.setCursorPos(1, ty)
	      term.clearLine()
	      term.setCursorPos(1, ty)
          Robot.Message("Waiting ["..tostring(countdown).."]")
	  	
	  	if countdown > 0 then
	  	  os.sleep(1)
	        countdown = countdown - 1
	  	end
	  	
	  	-- WHILE WE ARE HERE CLEANUP THE FIELD --
	  	  App.Farm.Vacuum()
	      
	      -- MOVE ROBOT DOWN THE LINE --
	      if l < App.Farm.LENGTH then
            success = Robot.MoveForwardEX() 
	      end
	    end
      
	  
	    -- TURN THE ROBOT FOR NEXT LINE --
	    if w < App.Farm.WIDTH then
	      if w % 2 == 0 then
            success = Robot.TurnLeft()
            success = Robot.MoveForwardEX()
            success = Robot.TurnLeft()
          else
            success = Robot.TurnRight()
            success = Robot.MoveForwardEX()
            success = Robot.TurnRight()
          end
	    end
	  end
	
	else
	  if countdown > 0 then
		os.sleep(1)
	    countdown = countdown - 1
      end
	end
	
	App.Farm.GotoOrigin()
  end
  
end



-- INTRODUCED: v0.1
function App.Farm.ValidateSeed(item)
  Robot.Debug("TODO: App.Farm.ValidateSeed")
  return true
end



-- INTRODUCED: v0.1
function App.Validate()
  local found_robot_api = false
  Robot = require("Robot")
  
  if Robot ~= nil then
    if Robot.VERSION >= App.ROBOT_VERSION then
      return true
	else
	  print("RUNTIME ERROR> Update Robot to v"..tonumber(App.ROBOT_VERSION))
    end
  end
  
  print("RUNTIME ERROR> Missing Robot v"..tonumber(App.ROBOT_VERSION))
  return false
end


-- APP ENTRY POINT --
if App.Validate() then
  return App.Main()
end