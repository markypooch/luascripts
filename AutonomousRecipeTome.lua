--[[
    Autonomous Recipe Tome
    -=-=-=-=-=-=-=-=-=-=-=-
    An Application to automate the creation of recipes as part of a supply line
]]--

hey = "0"
local Robot = require "Robot"

function main()

    print("")
    print("")
    print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
    print(":   Autonomous Recipe Tome    :")
    print(":   TurtleDom (c), 2018       :")                      
    print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
    print("Press Any key to continue")
    read()

    playerInput = 0
    repeat
        --Robot.ClearScreen()
        print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
        print(": 1. Enter New Recipe         :")
        print(": 2. List Known Recipes       :")
        print(": 3. Autonomous Mode          :")
        print(": 4. Quit                     :")
        print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
        print("Enter Choice")

        playerInput = tonumber(read())
        if   playerInput == 1 then
            getRecipeFromUser()
        elseif playerInput == 2 then
            listKnownRecipes()
        elseif playerInput == 3 then
            enterAutonomousMode()
        end

    until (playerInput == 4)
end

function listKnownRecipes()
    file     = io.open("recipes.txt", "r")
    if file ~= nil then
        lines    = Robot.ReadLines("recipes.txt")
        if lines ~= nil then
            for i=1, #lines, 1 do
                print(lines[i])
                if string.match(lines[i], "Recipe Name") then read() end
            end
        end
    end
end

function deleteARecipe()
end

function enterAutonomousMode()
    print("")
    print("-=-=-=-=-=-=-=-=-=-=-=-=-=-")
    print("The turtle is preparing to enter")
    print("Autonomous Mode. This Mode only")
    print("ends if the application is halted")
    print("-=-=-=-=-=-=-=-=-=-=-=-=-=-")
    print("Do you wish to continue? (y/n)")

    playerInput = read()
    if playerInput == 'y' or playerInput == 'Y' then
        print("Enter the item name")
        itemName = read()

        file     = io.open("recipes.txt", "r")
        if file ~= nil then
            lines    = Robot.ReadLines("recipes.txt")
            if lines ~= nil then
                nameFound = false
                idx = 0  
                for lineCount = 1, #lines, 1 do 
                    if string.match(lines[lineCount], itemName) then
                        print("Recipe Found!")
                        read()
                        print("")
                        nameFound = true
                        idx       = lineCount
                        break
                    end
                end

                if nameFound == true then
                    print("Parsing Ingredients")
                    ingredientsArray    = {}
                    manifestArray       = {}
                    
                    print(idx)
                    lineIdx = tonumber(idx) + 1
                    print(lines[lineIdx])
                    while not string.match(lines[lineIdx], "Recipe Name") do
                        print("hey")
                        strArray         = Robot.StringSplit(lines[lineIdx], "-")
                        print(tostring(strArray[1]))
                        table.insert(ingredientsArray, tostring(strArray[1].."/"..strArray[2]))
                        lineIdx = lineIdx + 1
                        if lineIdx > #lines then break end
                    end

                    print("Parsing Ingredients Complete, Manifest: ")
                    
                    for i=1, #ingredientsArray, 1 do
                        print(ingredientsArray[i])
                        print("length:")
                        print(#ingredientsArray)
                    end
                    
                    read()
                    print("Calculating Adjacent Ingredients...")

                    print("Clearing out inventory...")
                    for i=1, 16, 1 do
                        turtle.select(i)
                        turtle.dropUp()
                    end
                    print("Inventory Cleared")

                    for i=0, 3, 1 do
                        turtle.suck(1)
                        turtle.turnRight()

                        slot_data = turtle.getItemDetail(turtle.getSelectedSlot())
                        
                        if slot_data then
                            print(slot_data.name..":"..tostring(i))
                           manifestArray[slot_data.name] = i
                        end

                        turtle.dropUp()
                    end

                    print("Calculated Adjacent Ingredients")
                    read()
                    

                    while true do
                        numberOfTurns = 0
                        lastItemName  = ""
                        unableToPoll  = false
                        
                        print(#ingredientsArray)
                        for i=1, #ingredientsArray, 1 do
                            quantity      = 0
                            itemArr       = Robot.StringSplit(ingredientsArray[i], "/")
                            numberOfTurns = manifestArray[itemArr[2]] 
                            print ("Polling "..itemArr[2].." from chest# "..tostring(numberOfTurns))
                            for j=0, numberOfTurns-1, 1 do
                                turtle.turnRight()
                            end
                            
                            while true do
                                turtle.select(tonumber(itemArr[1]))
                                if not turtle.suck(1) then
                                    print("Waiting on item in slot "..itemArr[1])
                                else
                                    unableToPoll = false
                                    break
                                end
                            end

                            for j=numberOfTurns-1, 0, -1 do
                                turtle.turnLeft()
                            end
                        end
                        if not unableToPoll then

                            turtle.select(16)
                            turtle.craft()

                            print("Item Crafted!")
                            print("")

                            turtle.dropDown()
                        end
                    end
                else
                    print("Recipe not found!")
                end
            end
        end
    end
end

function getRecipeFromUser()
    print("Please enter the name of the item")
    itemName = read()

    file     = io.open("recipes.txt", "r")
    if file ~= nil then
        lines    = Robot.ReadLines("recipes.txt")
        if lines ~= nil then
            local nameFound = false  
            for lineCount = 1, #lines, 1 do 
                if string.match(lines[lineCount], itemName) then
                    print("Error: Line# "..tostring(lineCount).." already contains that Recipe!")
                    read()
                    print("")
                    nameFound = true
                end
            end

            file = io.open("recipes.txt", "a")
            if nameFound == false then
                file = io.open("recipes.txt", "a")
                if file ~= nil then
                    file:write("Recipe Name:"..itemName.."")
                    store(file)
                    file:close()
                end
            end

        else
            print("Error: Lines returned from file is nil")
            read()
            print("")
        end
    else 
        print("Creating recipes file...")
        file = io.open("recipes.txt", "a")
        if file == nil then
            print("Error: Couldn't create file")
            return false
        end
        file:write("
Recipe Name:"..itemName.."
")
        store(file)
        file:close()
    end

    return true
end

function store(file)
    print("Preparing to read current inventory")
    slotsToNotCheck = {4, 8, 12, 13, 14, 15, 16}
    for slot_index=1, 16, 1
    do
        local matched = false
        for sln_index = 1, #slotsToNotCheck, 1 do
            if slot_index == slotsToNotCheck[sln_index] then
                matched = true
                break
            end
        end
        if matched == false then

            turtle.select(slot_index)
            slot_data = turtle.getItemDetail(slot_index)

            if slot_data then
                stringToWrite = "
-"..tostring(slot_index).."-"..slot_data.name.."-"
                file:write(stringToWrite, "")
            end
        end
    end
    print("Recipe stored!")
    print("")
end

function craft()
end

main()