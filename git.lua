local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/' -- You will need this for encoding/decoding

function dec(data)
   data = string.gsub(data, '[^'..b..'=]', '')
   return (data:gsub('.', function(x)
       if (x == '=') then return '' end
       local r,f='',(b:find(x)-1)
       for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
       return r;
   end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
       if (#x ~= 8) then return '' end
       local c=0
       for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
           return string.char(c)
   end))
end

function dump(o)
   if (type(o) == 'table') then
      local s = '{'
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s..'['..k..']='..dump(v)..','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function mysplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
       table.insert(t, str)
    end
    return t    
end

function restful_request(url, method, body, content_type, token)
    headers = {}
    headers['Content-Type']  =content_type
    headers['PRIVATE-TOKEN'] =""..token..""
    headers['content-encoding'] = "identity"

    response = nil
    err      = nil
    if (method == "GET") then
        print(url)
        response, err = http.get(url, headers)
    elseif (method == "PUT") then
        print(http)
        response, err = http.put(url, headers) 
    else
        print(url)
        print(body)
        response, err = http.post(url, body, headers)
    end
    
    str = dump(http)
    
    filed = io.open("dump.txt", "w")
    filed:write(str)
    filed:close()
    if (err ~= nil) then
        print("Error executing restful request")
        print(err)
        return nil
    end
    
    return response
   
end

function main()

   if (arg[1] == "config") then
      configfilecontent = ""..arg[2]..":"..arg[3]..""
      file = io.open("gitconfig.txt","w")
      file:write(configfilecontent)
      io.close(file)      
   end
      

   gitconf = io.open("gitconfig.txt", "r")
   if (gitconf == nil) then
      print("Run `git config <token> <remote repo url> first`")
      return
   end
   
   confcontent = gitconf:read("*all")
   conf = mysplit(confcontent, ":")

   action = ""
   local cwd = shell.dir()
   
   if (arg[1] == "create" or arg[1] == "update") then
      file = io.open(""..cwd.."/"..arg[2].."", 'r')
      content = file:read("*all")
  
      content = string.gsub(content, "\n", "\\n")
      content = string.gsub(content, "\r", "\\r")
      content = string.gsub(content, "\t", "\\t")
      content = string.gsub(content, '"', '\\"')
       
      io.close(file)
      
      branch = arg[3]
      message = arg[4]
      action = '{"branch":"'..branch..'", "commit_message":"'..message..'", "actions":[{"action":"'..arg[1]..'", "file_path":"'..arg[2]..'", "content":"'..content..'"}]}'
         
      response = restful_request("https://gitlab.com/api/v4/projects/"..conf[2].."/repository/commits", "POST", action, 'application/json', conf[1])
      if (response == nil) then
           return
      end
      
   elseif (arg[1] == "pull") then
      response = restful_request("https://gitlab.com/api/v4/projects/"..conf[2].."/repository/files/"..arg[2].."?ref="..arg[3].."", "GET", nil, 'application/json', conf[1])
      if (response == nil) then
          return
      end
      
      
      ret = response.readAll()
      ret = string.gsub(ret, '{', '')
      ret = string.gsub(ret, '}', '')

      json_items = mysplit(ret, ",")
      last_item = mysplit(json_items[10], ":")
  
      -- decode the content from the pull
      file_data = dec(last_item[2])
      
      if (fs.exists(""..cwd.."/"..arg[2].."")) then
          if (not fs.exists(""..cwd.."/legacy")) then
              fs.makeDir(""..cwd.."/legacy")
          end
          if (fs.exists(""..cwd.."/legacy/"..arg[2].."")) then
              fs.delete(""..cwd.."/legacy/"..arg[2].."")    
          end
      
          fs.copy(""..cwd.."/"..arg[2].."", ""..cwd.."/legacy/"..arg[2].."")
          fs.delete(""..cwd.."/"..arg[2].."")
      end
      file_out = io.open(""..cwd.."/"..arg[2].."", "w")
      file_out:write(file_data)
      file_out:close()
      
      print("Successfully pulled git file: "..arg[2].."")
      
   elseif (arg[1] == "branch_create") then
      response = restful_request("https://gitlab.com/api/v4/projects/"..conf[2].."/repository/branches", "POST", "branch="..arg[2].."&ref="..arg[3].."", 'application/x-www-form-urlencoded', conf[1])
      
      if (response == nil) then
          return
      end
      
      print("Successfully created branch "..arg[2].." on remote")
   elseif (arg[1] == "branch_merge") then
      actions = '{"id":"'..arg[2]..'", "source_branch":"'..arg[3]..'", "target_branch":"'..arg[4]..'", "title":"'..arg[5]..'", "description":"'..arg[6]..'"}'
      response = restful_request("https://gitlab.com/api/v4/projects/"..conf[2].."/merge_requests", "POST", actions, "application/json", conf[1])
      if (response == nil) then
          return
      end
      
      file = io.open("merge_request.txt", "w")
      file:write(response.readAll())
      file:close()
   end
end

main()