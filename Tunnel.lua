local args = {...}
local Tunnel = { VERSION = 1.4, ROBOT_VERSION = 0.27 }
local Robot = nil

-- FUNCTION INTRODUCED AS OF VERSION 1.4
function Tunnel.Main()
  Robot.ClearScreen()
  Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
  Robot.Message("Tunnel version "..Tunnel.VERSION)
  Robot.Message("(c) Boxporium 2019-2020")
  Robot.Message("Author(s): Neometron")
  Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")

  if Tunnel.user_setup_enabled then
    Tunnel.UserSetup()
  end
  
  if Tunnel.ApplyOriginOffset() then
    
    Robot.DigForward()
    Robot.MoveForwardEX()

    -- START MAIN LOOP 
    while(Tunnel.COMPLETE == false) do
      Robot.Message("Tunneling ".. Tunnel.CURRENT_LENGTH .." of ".. Tunnel.LENGTH)
      
      if ( Tunnel.CURRENT_LENGTH - 1 ) % 4 == 0 then
        Tunnel.DigNarrow()
      else
        Tunnel.DigWide()
      end
    
      Tunnel.COMPLETE = Tunnel.AdvanceForward() == false

    end
    -- END MAIN LOOP
  
    if Tunnel.ReturnToOrigin() then
      if Tunnel.RemoveOriginOffset() then
        if Tunnel.IsBelowChest() then
          if Tunnel.torch_enabled then
            for slot_id = 1, 15 do
              Robot.InventorySelect(slot_id)
              --TODO Add function to Robot API Robot.PushInventory(direction, amount)
              while turtle.dropDown(64) == false and Tunnel.GetInventoryCount() > 0 do
                Robot.Message("OUTPUT INVENTORY FULL")
                Robot.Message("USER> Empty output chest")
                Robot.AnyKeyContinue()
              end
            end     
          else
            for slot_id = 1, 16 do
              Robot.InventorySelect(slot_id)
              --TODO Add function to Robot API Robot.PushInventory(direction, amount)
              while turtle.dropDown(64) == false and Tunnel.GetInventoryCount() > 0 do
                Robot.Message("OUTPUT INVENTORY FULL")
                Robot.Message("USER> Empty output chest")
                Robot.AnyKeyContinue()
              end
            end     
          end
        end
      end
    end

  end

  return true
end


-- FUNCTION INTRODUCED AS OF VERSION 1.4
function Tunnel.Initialize()
  -- ROBOT API CHECK INTRODUCED AS OF VERSION 1.4
  local found_robot_api = false
  Robot = require("Robot")
  if Robot ~= nil then
    if Robot.VERSION >= Tunnel.ROBOT_VERSION then
      found_robot_api = true
    end
  end
  if found_robot_api == false then
    print("RUNTIME ERROR> Missing Robot v"..tonumber(Tunnel.ROBOT_VERSION))
    return false
  end

  -- USER SETUP VARIABLE INTRODUCED AS OF VERSION 1.4
  Tunnel.user_setup_enabled = false
  if Tunnel.ArgSetup() == false then
    Tunnel.user_setup_enabled = true
  end

  -- LENGTH VARIABLES INTRODUCED AS OF VERSION 1.3
  Tunnel.LENGTH = nil
  Tunnel.CURRENT_LENGTH = 1
  Tunnel.COMPLETE = false

  -- OFFSET VARIABLES INTRODUCED AS OF VERSION 1.3
  Tunnel.offset_applied = false
  Tunnel.offset = vector.new( 0, 0, 0)
  Tunnel.offset_direction = Robot.DIRECTION.NORTH

  -- TORCH VARIABLE INTRODUCED AS OF VERSION 1.4
  Tunnel.torch_enabled = false

  return true
end


-- FUNCTION INTRODUCED AS OF VERSION 1.4
function Tunnel.ArgSetup()
  --TODO Process program arguments
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.4
function Tunnel.UserSetup()
  -- LENGTH SETUP INTRODUCED AS OF VERSION 1.3
  while Tunnel.LENGTH == nil do
    Tunnel.LENGTH = Robot.InputNumber("Tunnel Length");
    if Tunnel.LENGTH == nil or Tunnel.LENGTH < 1 then
      Robot.Error("Invalid Int [l > 0]")
      Tunnel.LENGTH = nil
    end
  end

  -- OFFSET X SETUP INTRODUCED AS OF VERSION 1.3
  local offset = nil
  while offset == nil do
    offset = Robot.InputNumber("X Offset");
    if offset == nil then
      Robot.Error("Invalid Int [ +-x ]")
      offset = nil
    end
  end
  Tunnel.offset.x = offset

  -- OFFSET Z SETUP INTRODUCED AS OF VERSION 1.3
  offset = nil
  while offset == nil do
    offset = Robot.InputNumber("Z Offset");
    if offset == nil or offset < 0 then
      Robot.Error("Invalid Int [z >= 0]")
      offset = nil
    end
  end
  Tunnel.offset.z = offset

  -- OFFSET DIRECTION SETUP INTRODUCED AS OF VERSION 1.3
  -- VERSION 1.4 ADDED: "n" "s" "e" "w"
  local offset_direction = nil
  while offset_direction == nil do
    offset_direction = Robot.InputString("Offset Direction")
    if     offset_direction == "north" or offset_direction == "n" then
      Tunnel.offset_direction = Robot.DIRECTION.NORTH
    elseif offset_direction == "south" or offset_direction == "s" then
      Tunnel.offset_direction = Robot.DIRECTION.SOUTH
    elseif offset_direction == "east"  or offset_direction == "e" then
      Tunnel.offset_direction = Robot.DIRECTION.EAST
    elseif offset_direction == "west"  or offset_direction == "w" then
      Tunnel.offset_direction = Robot.DIRECTION.WEST
    else
      Robot.Error("[n|s|e|w]")
      Robot.Error("[north|south|east|west]")
      offset_direction = nil
    end
  end

  -- TORCH SETUP INTRODUCED AS OF VERSION 1.4
  --local add_torches = nil
  --while add_torches == nil do
  --  add_torches = Robot.InputString("Add Torches (SLOT 16)")
  --  if add_torches == "y" or add_torches == "yes" then
  --    Tunnel.torch_enabled = true
  --  elseif add_torches == "n" or add_torches == "no" then
  --    Tunnel.torch_enabled = false
  --  else
  --    Robot.Error("[y|yes|n|no]")
  --    add_torches = nil
  --  end
  --end
  Tunnel.PullInventory_Torch()

  return true
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.CheckInventoryFull()
  local slots_used = 0
  
  for i = 1, 16 do
    if ( turtle.getItemCount(i) > 0 ) then
      slots_used = slots_used + 1
    end
  end
  
  if ( slots_used == 16 ) then
    --Robot.Debug("Inv slots used: " .. tostring(slots_used))
    return true
  end
  
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.ApplyOriginOffset()
  if ( Tunnel.offset_applied == false ) then
    --Robot.Debug("Applying offset")
    
	local dist_x = Tunnel.offset.x
	local dist_z = Tunnel.offset.z
	
	if ( dist_x < 0 ) then dist_x = -dist_x end
	
	-- MOVE ROBOT FORWARD ON Z AXIS
	local move_z = 0
	while (move_z < dist_z) do
	  --Robot.Debug("move_z = " .. tostring(move_z))
      Robot.DigForward()
      if(Robot.MoveForwardEX()) then move_z = move_z + 1 end
	end
        
    -- MOVE ROBOT FORWARD ON X AXIS
	if ( Tunnel.offset.x < 0 ) then Robot.TurnLeft() end
    if ( Tunnel.offset.x > 0 ) then Robot.TurnRight() end
	
	local move_x = 0
	while ( move_x < dist_x ) do
	  --Robot.Debug("move_x = " .. tostring(move_x))
      Robot.DigForward()
      if(Robot.MoveForwardEX()) then move_x = move_x + 1 end
	end
	
    -- TURN TO OFFSET DIRECTION
    Tunnel.offset_mark_direction = Robot.direction
    Tunnel.offset_mark_turns = 0
    
    if ( Tunnel.offset_direction == Robot.DIRECTION.NORTH ) then
      if ( Robot.direction == Robot.DIRECTION.EAST ) then
        Robot.TurnLeft()
        Tunnel.offset_mark_turns = -1
      elseif ( Robot.direction == Robot.DIRECTION.SOUTH ) then
        Robot.TurnLeft()
        Robot.TurnLeft()
        Tunnel.offset_mark_turns = -2
      elseif ( Robot.direction == Robot.DIRECTION.WEST ) then
        Robot.TurnRight()
        Tunnel.offset_mark_turns = 1
      else
        -- Already Facing North
        Tunnel.offset_mark_turns = 0
      end
    elseif ( Tunnel.offset_direction == Robot.DIRECTION.EAST ) then
      if ( Robot.direction == Robot.DIRECTION.NORTH ) then
        Robot.TurnRight()
        Tunnel.offset_mark_turns = 1
      elseif ( Robot.direction == Robot.DIRECTION.WEST ) then
        Robot.TurnRight()
        Robot.TurnRight()
        Tunnel.offset_mark_turns = 2
      elseif ( Robot.direction == Robot.DIRECTION.SOUTH ) then
        Robot.TurnLeft()
        Tunnel.offset_mark_turns = -1
      else
        -- Already Facing East
        Tunnel.offset_mark_turns = 0
      end
    elseif ( Tunnel.offset_direction == Robot.DIRECTION.WEST ) then
      if ( Robot.direction == Robot.DIRECTION.NORTH ) then
        Robot.TurnLeft()
        Tunnel.offset_mark_turns = -1
      elseif ( Robot.direction == Robot.DIRECTION.SOUTH ) then
        Robot.TurnRight()
        Tunnel.offset_mark_turns = 1
      elseif ( Robot.direction == Robot.DIRECTION.EAST ) then
        Robot.TurnRight()
        Robot.TurnRight()
        Tunnel.offset_mark_turns = 2
      else
        -- Already Facing West
        Tunnel.offset_mark_turns = 0
      end      
    elseif ( Tunnel.offset_direction == Robot.DIRECTION.SOUTH ) then
      if ( Robot.direction == Robot.DIRECTION.WEST ) then
        Robot.TurnLeft()
        Tunnel.offset_mark_turns = -1
      elseif ( Robot.direction == Robot.DIRECTION.NORTH ) then
        Robot.TurnLeft()
        Robot.TurnLeft()
        Tunnel.offset_mark_turns = -2
      elseif ( Robot.direction == Robot.DIRECTION.EAST ) then
        Robot.TurnRight()
        Tunnel.offset_mark_turns = 1
      else
        -- Already Facing South
        Tunnel.offset_mark_turns = 0
      end
	else
	  Robot.Error("Invalid Tunnel.offset_direction")
    end
      
    -- SET ROBOT ORIGIN HERE
    Robot.position = vector.new( 0, 0, 0)
    Robot.direction = Robot.DIRECTION.NORTH 
    
    Tunnel.offset_applied = true
    Robot.Message("Offset applied")
    return true
  
  end
  Robot.Error("Offset already applied")
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.RemoveOriginOffset()
  if ( Tunnel.offset_applied ) then
    Robot.Message("Removing Offset")
	
	Robot.TurnLeft()
	Robot.TurnLeft()
    
	-- DETERMINE DISTANCES    
    local dist_x = Tunnel.offset.x
    local dist_z = Tunnel.offset.z
	
	if ( dist_x < 0 ) then dist_x = -dist_x end
	--Robot.Debug("dist_x = " .. tostring(dist_x))
	--Robot.Debug("dist_z = " .. tostring(dist_z))
	
    -- REVERT ROTATION
	--Robot.Debug("Tunnel.offset_mark_turns = " .. tostring(Tunnel.offset_mark_turns))
    if ( Tunnel.offset_mark_turns < 0 ) then
      local req_turns = -Tunnel.offset_mark_turns
      for turns = 0, req_turns do
        Robot.TurnLeft()
      end      
    end
    if ( Tunnel.offset_mark_turns > 0 ) then
      local req_turns = Tunnel.offset_mark_turns
      for turns = 0, req_turns do
        Robot.TurnRight()
      end
    end
    
    -- MOVE ON X
	local move_x = 0
	while (move_x < dist_x) do
      --Robot.DigForward()
      Robot.MoveForwardEX()
	  move_x = move_x + 1
    end
    
    -- TURN 
    if ( Tunnel.offset.x < 0 ) then
      Robot.TurnRight()
    elseif ( Tunnel.offset.x > 0 ) then
      Robot.TurnLeft()
    end
    
    -- MOVE ON Z
	local move_z = 0
	while (move_z < dist_z) do
      --Robot.DigForward()
      Robot.MoveForwardEX()
	  move_z = move_z + 1
    end
    
    -- FINAL TURNS
    Robot.TurnLeft()
    Robot.TurnLeft()
    
    -- RESET ROBOT ORIGIN
    Robot.position = vector.new( 0, 0, 0 )
    Robot.direction = Robot.DIRECTION.NORTH    
    
    Tunnel.offset_applied = false
    Robot.Message("Offset removed")
    return true
  else
	Robot.Message("Offset already removed")
  end
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.ReturnToOrigin()
  --Robot.Debug("Tunnel.ReturnToOrigin()")

  Tunnel.mark_direction = Robot.direction 
  Tunnel.mark_position  = Robot.position

  -- MAKE THE TURTLE FACE SOUTH  
  if ( Robot.direction == Robot.DIRECTION.NORTH ) then
    Robot.TurnLeft()
    Robot.TurnLeft()
  elseif ( Robot.direction == Robot.DIRECTION.EAST ) then
    Robot.TurnRight()
  elseif ( Robot.direction == Robot.DIRECTION.WEST ) then
    Robot.TurnLeft()
  end
  
  --MOVE FORWARD OUT OF WORK AREA
  --if (Robot.position.z ~= Robot.origin.z) then
    Robot.MoveForwardEX()
  --end
  
  --MOVE TO ORIGIN
  if (Robot.position.x ~= Robot.origin.x) or (Robot.position.y ~= Robot.origin.y) or (Robot.position.z ~= Robot.origin.z) then
	  --MOVE TO ORIGIN Y AXIS
	  while ( Robot.position.y < Robot.origin.y ) do
	    Robot.DigUp()
		Robot.MoveUpEX()
	  end
	  while ( Robot.position.y > Robot.origin.y ) do
	    Robot.DigDown()
		Robot.MoveDownEX()
	  end
	  --MOVE TO ORIGIN X AXIS
	  if ( Robot.position.x < Robot.origin.x ) then
		Robot.TurnLeft()
		Robot.DigForward()
		Robot.MoveForwardEX()
		Robot.TurnRight()
	  end
	  if ( Robot.position.x > Robot.origin.x ) then
		Robot.TurnRight()
		Robot.DigForward()
		Robot.MoveForwardEX()
		Robot.TurnLeft()
	  end               						  
	  --MOVE DOWN ORIGIN Z AXIS
	  --Robot.Debug("MOVE DOWN ORIGIN Z AXIS")
	  --Robot.AnyKeyContinue()
	  --Robot.Debug("rz: " .. tostring(Robot.position.z) .. "ro: " .. tostring(Robot.origin.z))
	  while ( Robot.position.z > Robot.origin.z ) do
		--Robot.Debug("rz: " .. tostring(Robot.position.z) .. "ro: " .. tostring(Robot.origin.z))
		Robot.MoveForwardEX()  
	  end
  end
  
  --RESET DIRECTION
  Robot.TurnLeft()
  Robot.TurnLeft()
    
  return true
  --return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.ReturnToWork()
  --Robot.Debug("Tunnel.ReturnToWork()")

  --MOVE NEAR WORKING AREA
  while ( Robot.position.z < ( Tunnel.mark_position.z - 1 ) ) do
    Robot.MoveForwardEX()
  end
  
  --GET ON LEVEL WITH WORKING AREA
  --Robot.Debug("Robot.position.y = " .. tostring(Robot.position.y))
  while ( Robot.position.y > Tunnel.mark_position.y ) do
    Robot.MoveDownEX()
  end
  while ( Robot.position.y < Tunnel.mark_position.y ) do
    Robot.MoveUpEX()
  end
  
  --TURN AND MOVE TOWARD WORKING AREA
  if ( Robot.position.x > Tunnel.mark_position.x ) then
    Robot.TurnLeft()
    Robot.MoveForwardEX()
    Robot.TurnRight()
  end
  if ( Robot.position.x < Tunnel.mark_position.x ) then
    Robot.TurnRight()
    Robot.MoveForwardEX()
    Robot.TurnLeft()
  end
  
  --MOVE INTO WORK AREA
  Robot.MoveForwardEX()
  
  --RESUME DIRECTION
  if ( Tunnel.mark_direction == Robot.DIRECTION.EAST ) then
    Robot.TurnRight()
  elseif ( Tunnel.mark_direction == Robot.DIRECTION.WEST ) then
    Robot.TurnLeft()
  end
  
  return true      
  --return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
-- VERSION 1.4: Updated using Robot.Look(direction) : Robot v0.27 
function Tunnel.IsBelowChest()
  return Robot.Look(Robot.DIRECTION.DOWN) == "minecraft:chest"
end


-- FUNCTION INTRODUCED AS OF VERSION 1.4
function Tunnel.IsInventoryTorch()
  return Robot.InventoryName() == "minecraft:torch"
end


-- FUNCTION INTRODUCED AS OF VERSION 1.4
function Tunnel.PlaceTorch()
  if Tunnel.torch_enabled then
    if Robot.InventorySelect(16) then
      local torch_attempt = 0
      while torch_attempt < 3 do
        if Tunnel.IsInventoryTorch() then
          return Robot.PlaceForward()
        else
          Tunnel.UnloadInventory()
          torch_attempt = torch_attempt + 1
        end
      end
      Tunnel.torch_enabled = false
    end
  end
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.4
function Tunnel.PullInventory_Torch()
  local result = false
  Robot.TurnRight()
  if Robot.Look(Robot.DIRECTION.FORWARD) == "minecraft:chest" then
    if Robot.InventorySelect(16) then
      -- First check if turtle has torches already in slot 16
      -- If not then dump contents from slot 16
      if Tunnel.IsInventoryTorch() == false and Tunnel.GetInventoryCount() > 0 then
        if Tunnel.IsBelowChest() then
          --TODO Add function to Robot API Robot.PushInventory(direction, amount)
          while turtle.dropDown(64) == false and Tunnel.GetInventoryCount() > 0 do
            Robot.Message("OUTPUT INVENTORY FULL")
            Robot.Message("USER> Empty output chest")
            Robot.AnyKeyContinue()
          end
        else
          -- Require User intervention to remove contents from inventory
          Robot.Message("NO OUTPUT INVENTORY")
          Robot.Message("USER> Unload Slot 16")
          Robot.AnyKeyContinue()
        end
      end
      
      -- Pull inventory from Torch Chest
      --TODO Add function to Robot API Robot.PullInventory(direction, amount)
      local pull_complete = false
      while pull_complete == false do
        if Tunnel.IsInventoryTorch() == false and Tunnel.GetInventoryCount() > 0 then
          Robot.Message("INVALID: "..Robot.InventoryName())
          -- Remove offending items
          if Tunnel.IsBelowChest() then
            --TODO Add function to Robot API Robot.PushInventory(direction, amount)
            while turtle.dropDown(64) == false and Tunnel.GetInventoryCount() > 0 do
              Robot.Message("OUTPUT INVENTORY FULL")
              Robot.Message("USER> Empty output chest")
              Robot.AnyKeyContinue()
            end
          else
            -- Require User intervention to remove contents from inventory
            Robot.Message("NO OUTPUT INVENTORY")
            Robot.Message("USER> Unload Slot 16")
            Robot.AnyKeyContinue()
          end
        
        elseif Tunnel.IsInventoryTorch() == true and Tunnel.GetInventoryCount() == 64 then
          pull_complete = true
        
        elseif Tunnel.IsInventoryTorch() == true and Tunnel.GetInventoryCount() < 64 then
          local inv_count = Tunnel.GetInventoryCount()
          local inv_request = 64 - inv_count
          while turtle.suck(inv_request) == false do
            Robot.Message("INPUT INVENTORY EMPTY")
            Robot.Message("USER> Fill torch chest")
            Robot.AnyKeyContinue()
          end
          pull_complete = true
        
        elseif Tunnel.GetInventoryCount() == 0 then
          while turtle.suck(64) == false do
            Robot.Message("INPUT INVENTORY EMPTY")
            Robot.Message("USER> Fill torch chest")
            Robot.AnyKeyContinue()
          end
        end
      end

      Tunnel.torch_enabled = Tunnel.IsInventoryTorch()
      result = true
      
    end
  else
    Tunnel.torch_enabled = false
  end
  Robot.TurnLeft()
  return result
end


-- FUNCTION INTRODUCED AS OF VERSION 1.4
-- TODO Add function to Robot API Robot.GetInventoryCount()
function Tunnel.GetInventoryCount()
  local slot_id = turtle.getSelectedSlot() 
  return turtle.getItemCount(slot_id)
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.IsInventoryEmpty()
  local is_empty = true
  for i = 1, 16 do
    if ( turtle.getItemCount(i) > 0 ) then
      is_empty = false
    end    
  end
  return is_empty
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
-- VERSION 1.4 ADDED: condition for Tunnel.torch_enabled
-- VERSION 1.4 ADDED: Call Tunnel.PullInventory_Torch()
function Tunnel.UnloadInventory()
  --Robot.Debug("Tunnel.UnloadInventory()")  
  if Tunnel.ReturnToOrigin() then
    if Tunnel.RemoveOriginOffset() then
      if Tunnel.IsBelowChest() then
        if Tunnel.torch_enabled then
          for slot_id = 1, 15 do
            Robot.InventorySelect(slot_id)
            --TODO Add function to Robot API Robot.PushInventory(direction, amount)
            while turtle.dropDown(64) == false and Tunnel.GetInventoryCount() > 0 do
              Robot.Message("OUTPUT INVENTORY FULL")
              Robot.Message("USER> Empty output chest")
              Robot.AnyKeyContinue()
            end
          end     
        else
          for slot_id = 1, 16 do
            Robot.InventorySelect(slot_id)
            --TODO Add function to Robot API Robot.PushInventory(direction, amount)
            while turtle.dropDown(64) == false and Tunnel.GetInventoryCount() > 0 do
              Robot.Message("OUTPUT INVENTORY FULL")
              Robot.Message("USER> Empty output chest")
              Robot.AnyKeyContinue()
            end
          end     
        end

        Tunnel.PullInventory_Torch()

        if Tunnel.ApplyOriginOffset() then
          if Tunnel.ReturnToWork() then
            return true
          end
        end
      else
        Robot.Message("NO OUTPUT INVENTORY")
        while Tunnel.IsInventoryEmpty() == false do
          Robot.Message("USER> Empty All Items")
          Robot.AnyKeyContinue()
        end
        return true
      end        
    end  
  end
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.AdvanceForward()

  if Tunnel.CURRENT_LENGTH == Tunnel.LENGTH then
	return false
  end
  
  Robot.DigForward()
  Robot.MoveForwardEX()
  
  Tunnel.CURRENT_LENGTH = Tunnel.CURRENT_LENGTH + 1
  
  return true
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.DigForward()

  if Tunnel.CheckInventoryFull() then
    Tunnel.UnloadInventory()
  end
  
  Robot.DigForward()
  
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.DigUp()

  if Tunnel.CheckInventoryFull() then
    Tunnel.UnloadInventory()
  end
  
  Robot.DigUp()

  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.DigDown()

  if Tunnel.CheckInventoryFull() then
    Tunnel.UnloadInventory()
  end
  
  Robot.DigDown()
  
  return false
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
-- VERSION 1.4 ADDED: Tunnel.PlaceTorch()
function Tunnel.DigNarrow()
  Tunnel.DigUp()
  Robot.MoveUpEX()
  Robot.TurnRight()
  Tunnel.DigForward()
  Robot.MoveForwardEX()
  Tunnel.DigDown()
  Robot.MoveDownEX()
  Tunnel.DigDown()
  Robot.MoveDownEX()
  Robot.TurnRight()
  Robot.TurnRight()
  Tunnel.DigForward()
  Robot.MoveForwardEX()
  Tunnel.DigForward()
  Robot.MoveForwardEX()
  Tunnel.DigUp()
  Robot.MoveUpEX()
  Tunnel.DigUp()
  Robot.TurnRight()
  Robot.TurnRight()
  Robot.MoveForwardEX()
  Tunnel.PlaceTorch()
  Robot.TurnLeft()
  return true
end


-- FUNCTION INTRODUCED AS OF VERSION 1.3
function Tunnel.DigWide()
  Tunnel.DigUp()
  Robot.MoveUpEX()
  Tunnel.DigUp()
  Robot.TurnRight()
  Tunnel.DigForward()
  Robot.MoveForwardEX()
  Tunnel.DigUp()
  Tunnel.DigForward()
  Tunnel.DigDown()
  Robot.MoveDownEX()
  Tunnel.DigForward()
  Tunnel.DigDown()
  Robot.MoveDownEX()
  Tunnel.DigForward()
  Robot.TurnRight()
  Robot.TurnRight()
  Tunnel.DigForward()
  Robot.MoveForwardEX()
  Tunnel.DigForward()
  Robot.MoveForwardEX()
  Tunnel.DigForward()
  Tunnel.DigUp()
  Robot.MoveUpEX()
  Tunnel.DigForward()
  Tunnel.DigUp()
  Robot.MoveUpEX()
  Tunnel.DigForward()
  Tunnel.DigUp()
  Robot.MoveDownEX()
  Robot.TurnRight()
  Robot.TurnRight()
  Robot.MoveForwardEX()
  Robot.TurnLeft()
  return true
end


-- START TUNNEL PROGRAM
if Tunnel.Initialize() then
  return Tunnel.Main()
else
  return false
end