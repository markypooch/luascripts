local args = {...}
local Quarry = { VERSION = 0.1, ROBOT_VERSION = 0.27 }
local Robot = nil

-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Initialize()
    -- CHECK DEPENDENCY: ROBOT API
    local found_robot_api = false
    Robot = require("Robot")
    if Robot ~= nil then
      if Robot.VERSION >= Quarry.ROBOT_VERSION then
        found_robot_api = true
      end
    end
    if found_robot_api == false then
      print("RT ERROR> Missing Robot v"..tonumber(Quarry.ROBOT_VERSION))
      return false
    end
  
    -- DEPTH VARIABLES INTRODUCED AS OF VERSION 0.1
    Quarry.LENGTH = nil
    Quarry.WIDTH  = nil
    Quarry.DEPTH  = nil

    Quarry.complete  = false
    Quarry.length = 1
    Quarry.width  = 1
    Quarry.depth  = 1
    Quarry.procedure = Quarry.Procedure_Length
    Quarry.procedure_reverse = (Quarry.depth - 1) % 2 == 1

    Quarry.mark_position  = nil
    Quarry.mark_procedure = nil
  
    return true
end --FUNCTION Quarry.Initialize()


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.ArgSetup()
    --TODO Process program arguments
    return false
end -- FUNCTION Quarry.ArgSetup()


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.UserSetup()
    -- LENGTH SETUP INTRODUCED AS OF VERSION 0.1
    while Quarry.LENGTH == nil do
        Quarry.LENGTH = Robot.InputNumber("Length");
        if Quarry.LENGTH == nil or Quarry.LENGTH < 1 then
            Robot.Error("Invalid [Lenght > 0]")
            Quarry.LENGTH = nil
        end
    end
    -- WIDTH SETUP INTRODUCED AS OF VERSION 0.1
    while Quarry.WIDTH == nil do
        Quarry.WIDTH = Robot.InputNumber("Quarry Width");
        if Quarry.WIDTH == nil or Quarry.WIDTH < 1 then
            Robot.Error("Invalid [Width > 0]")
            Quarry.WIDTH = nil
        end
    end
    -- DEPTH SETUP INTRODUCED AS OF VERSION 0.1
    while Quarry.DEPTH == nil do
        Quarry.DEPTH = Robot.InputNumber("Quarry Depth");
        if Quarry.DEPTH == nil or Quarry.DEPTH < 1 then
            Robot.Error("Invalid Int [l > 0]")
            Quarry.DEPTH = nil
        end
    end
    return true
end -- FUNCTION Quarry.UserSetup()


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.WriteLine(line_pos, tEXt)
    term.setCursorPos(1, line_pos)
    term.clearLine()
    Robot.Message(tEXt)
end

-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Main()
    Robot.ClearScreen()
    Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
    Robot.Message("Quarry version "..Quarry.VERSION)
    Robot.Message("(c) Boxporium 2020")
    Robot.Message("Author(s): Neometron")
    Robot.Message("=-=-=-=-=-=-=-=-=-=-=-=-=-=")
    if Quarry.ArgSetup() == false then
        if Quarry.UserSetup() == false then
            return false
        end
    end
    Robot.ClearScreen()

    -- START MAIN LOOP 
    while Quarry.procedure ~= Quarry.Procedure_Quit do
        -- VALUE OUT OF BOUNDS CHECKS
        if Quarry.length < 1 or Quarry.length > Quarry.LENGTH then
            error("Quarry.lenth OOB:"..tostring(Quarry.length)) 
        end
        if Quarry.width < 1 or Quarry.width > Quarry.WIDTH then
            error("Quarry.width OOB:"..tostring(Quarry.width)) 
        end
        if Quarry.depth < 1 or Quarry.depth > Quarry.DEPTH then
            error("Quarry.depth OOB:"..tostring(Quarry.depth)) 
        end

        Quarry.WriteLine(1, "=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Quarry.WriteLine(2, "Quarry version "..Quarry.VERSION)
        Quarry.WriteLine(3, "(c) Boxporium 2020")
        Quarry.WriteLine(4, "Author(s): Neometron")
        Quarry.WriteLine(5, "=-=-=-=-=-=-=-=-=-=-=-=-=-=")
        Quarry.WriteLine(6, "L: "..tostring(Quarry.length))
        Quarry.WriteLine(7, "W: "..tostring(Quarry.width))
        Quarry.WriteLine(8, "Depth:"..Quarry.depth.." of ".. Quarry.DEPTH)
        Quarry.WriteLine(9, "Quarry.complete: "..tostring(Quarry.complete))

        Quarry.procedure_reverse = (Quarry.depth - 1) % 2 == 1
        
        if Quarry.procedure() == false then
            error("Procedure Failed")
        end
    end
    -- END MAIN LOOP
    return true
end -- FUNCTION Quarry.Main()


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Procedure_Quit()
    Quarry.WriteLine(10, "MODE: Procedure_Quit")
    Quarry.WriteLine(11, "")
    Quarry.procedure = Quarry.Procedure_Quit
    return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Mark()
    Quarry.mark_procedure = Quarry.procedure
    Quarry.mark_position  = { Quarry.length, Quarry.width, Quarry.depth, Robot.direction }
    return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Procedure_Length()
    Quarry.WriteLine(10, "MODE: Procedure_Length")
    if Robot.direction == Robot.DIRECTION.NORTH then
        if Quarry.length == Quarry.LENGTH then
            Quarry.procedure = Quarry.Procedure_Width
            return true
        end
        if Quarry.length < Quarry.LENGTH then
            Quarry.Dig()
            if Quarry.procedure == Quarry.Procedure_ReturnToOrigin then
                return true
            end
            Robot.MoveForwardEX()
            Quarry.length = Quarry.length + 1
            return true
        end
    elseif Robot.direction == Robot.DIRECTION.SOUTH then
        if Quarry.length == 1 then
            Quarry.procedure = Quarry.Procedure_Width
            return true
        end
        if Quarry.length > 1 then
            Quarry.Dig()
            if Quarry.procedure == Quarry.Procedure_ReturnToOrigin then
                return true
            end
            Robot.MoveForwardEX()
            Quarry.length = Quarry.length - 1
            return true
        end
    else
        error("Robot.direction:"..tostring(Robot.direction)) 
    end
    return false
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Procedure_Width()
    local direction_forward  = nil
    local direction_backward = nil
    local direction_modifier = nil
    if Quarry.procedure_reverse == false then
        if Quarry.width == Quarry.WIDTH then
            Quarry.procedure = Quarry.Procedure_Depth
            return true
        end
        Quarry.WriteLine(9, "MODE: Procedure_Width Forward")
        direction_forward  = Robot.DIRECTION.NORTH
        direction_backward = Robot.DIRECTION.SOUTH
        direction_modifier = 1
    else
        if Quarry.width == 1 then
            Quarry.procedure = Quarry.Procedure_Depth
            return true
        end
        Quarry.WriteLine(9, "MODE: Procedure_Width Backward")
        direction_forward  = Robot.DIRECTION.SOUTH
        direction_backward = Robot.DIRECTION.NORTH
        direction_modifier = -1
    end

    Quarry.WriteLine(11, "dir_f: "..tostring(direction_forward).." dir_b: "..tostring(direction_backward).." dir_m: "..tostring(direction_modifier))
    --Robot.AnyKeyContinue()

    if Robot.direction == direction_forward then
        Robot.TurnRight()
        Quarry.Dig()
        if Quarry.procedure == Quarry.Procedure_ReturnToOrigin then
            Robot.TurnLeft()
            return true
        end
        Robot.MoveForwardEX()
        Quarry.width = Quarry.width + direction_modifier
        Robot.TurnRight()
        Quarry.procedure = Quarry.Procedure_Length
        return true
    elseif Robot.direction == direction_backward then
        Robot.TurnLeft()
        Quarry.Dig()
        if Quarry.procedure == Quarry.Procedure_ReturnToOrigin then
            Robot.TurnRight()
            return true
        end
        Robot.MoveForwardEX()
        Quarry.width = Quarry.width + direction_modifier
        Robot.TurnLeft()
        Quarry.procedure = Quarry.Procedure_Length
        return true
    else
        error("Robot.direction:"..tostring(Robot.direction)) 
    end
    
    return false
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Procedure_Depth()
    Quarry.WriteLine(10, "MODE: Procedure_Depth")
    if Quarry.depth == Quarry.DEPTH then
        Quarry.complete = true
        Quarry.Mark()
        Quarry.procedure = Quarry.Procedure_ReturnToOrigin
        return true
    end
    Quarry.Dig(Robot.DIRECTION.DOWN)
    if Quarry.procedure == Quarry.Procedure_ReturnToOrigin then
        return true
    end
    if Quarry.procedure_reverse then
        Robot.TurnRight()
        Robot.TurnRight()
    else
        Robot.TurnLeft()
        Robot.TurnLeft()
    end
    Robot.MoveDownEX()
    Quarry.depth = Quarry.depth + 1
    Quarry.procedure = Quarry.Procedure_Length
    return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Procedure_ReturnToOrigin()
    Quarry.WriteLine(10, "MODE: Procedure_ReturnToOrigin")
    
    while Quarry.depth ~= 1 do
        Robot.MoveUpEX()
        Quarry.depth = Quarry.depth - 1
    end

    if Robot.direction == Robot.DIRECTION.NORTH then
        Robot.TurnLeft()
        Robot.TurnLeft()
    end

    while Quarry.length > 1 do
        Robot.MoveForwardEX()
        Quarry.length = Quarry.length - 1
    end

    if Quarry.width > 1 then
        Robot.TurnRight()
        while Quarry.width > 1 do
            Robot.MoveForwardEX()
            Quarry.width = Quarry.width - 1
        end
        Robot.TurnLeft()
    end

    Quarry.procedure = Quarry.Procedure_PushInventory
    return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Procedure_ReturnToWork()
    Quarry.WriteLine(10, "MODE: Procedure_ReturnToWork")
    
    if Quarry.complete then
        Robot.TurnLeft()
        Robot.TurnLeft()
        return Quarry.Procedure_Quit()
    end

    if Quarry.mark_procedure == nil or Quarry.mark_position == nil then
        error("Invalid mark")
    end

    if Quarry.mark_position[2] > 1 then
        Robot.TurnLeft()
        while Quarry.width < Quarry.mark_position[2] do
            Robot.MoveForwardEX()
            Quarry.width = Quarry.width + 1
        end
        Robot.TurnLeft()
    else
        Robot.TurnLeft()
        Robot.TurnLeft()
    end

    while Quarry.length < Quarry.mark_position[1] do
        Robot.MoveForwardEX()
        Quarry.length = Quarry.length + 1
    end

    if Quarry.mark_position[4] == Robot.DIRECTION.SOUTH then
        Robot.TurnLeft()
        Robot.TurnLeft()
    end

    while Quarry.depth < Quarry.mark_position[3] do
        Robot.MoveDownEX()
        Quarry.depth = Quarry.depth + 1
    end

    Quarry.procedure = Quarry.mark_procedure
    Quarry.mark_procedure = nil
    Quarry.mark_position  = nil
    return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.IsChest(direction)
    local direction = direction or Robot.DIRECTION.FORWARD
    local minecraft_id = Robot.Look(direction)
    if minecraft_id == "minecraft:chest"
    --or minecraft_id == "something else"
    then return true end
    return false
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Procedure_PushInventory()
    Quarry.WriteLine(10, "MODE: Procedure_PushInventory")
    if Quarry.IsChest(Robot.DIRECTION.FORWARD) then
        for slot_id = 1, 16 do
            Robot.InventorySelect(slot_id)
            --TODO Add function to Robot API Robot.PushInventory(direction, amount)
            while turtle.drop(64) == false and Quarry.GetInventoryCount() > 0 do
              Robot.Message("ERROR: OUTPUT INVENTORY FULL")
              Robot.Message("USER> Empty output chest")
              Robot.AnyKeyContinue()
            end
        end
    else
        Robot.Message("ERROR: NO OUTPUT INVENTORY")
        repeat
            Robot.Message("USER> Empty All Items")
            Robot.AnyKeyContinue()
        until Quarry.IsInventoryEmpty()
    end
    Robot.ClearScreen()
    Quarry.procedure = Quarry.Procedure_ReturnToWork
    return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.Dig(direction)
    local direction = direction or Robot.DIRECTION.FORWARD
    if Quarry.IsInventoryFull() then
        Quarry.Mark()
        Quarry.procedure = Quarry.Procedure_ReturnToOrigin
        return false
    end
    return Robot.Dig(direction)
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
-- TODO Add function to Robot API Robot.GetInventoryCount()
function Quarry.GetInventoryCount()
    local slot_id = turtle.getSelectedSlot() 
    return turtle.getItemCount(slot_id)
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.IsInventoryEmpty()
    local item_count = 0
    for slot_id = 1, 16 do
        if Robot.InventorySelect(slot_id) then
            if Quarry.GetInventoryCount() > 0 then return false end
        else
            error("Robot.InventorySelect("..tostring(slot_id)..")")
        end
    end
    return true
end


-- FUNCTION INTRODUCED AS OF VERSION 0.1
function Quarry.IsInventoryFull()
    local slots_used = 0
    for slot_id = 1, 16 do
        --This is faster than Quarry.GetInventoryCount()
      if ( turtle.getItemCount(slot_id) > 0 ) then
        slots_used = slots_used + 1
      end
    end
    return slots_used == 16
end


-- START PROGRAM
if Quarry.Initialize() then return Quarry.Main() end
return false