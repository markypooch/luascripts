--Random Comment
local ARGS = {...}
local TCM = {}
local Robot = nil
local TreeCutter = nil

-- REQUIRED TREECUTTER MODULE VARIABLE
TCM.TreeCutterVersion = 0.1
TCM.TreeName = "Redwood"


function TCM.TestWood(minecraft_id)
	return minecraft_id == "biomesoplenty:redwood_log"
end


function TCM.TestAir(minecraft_id)
	return minecraft_id == "minecraft:air"
end


function TCM.StartProcedure()
	Robot.Message(TCM.TreeName.."> Start")
	Robot.Dig(Robot.DIRECTION.FORWARD)
	Robot.MoveForwardEX()
	TCM.trunk_distance = 0
	TCM.current_branch = 0
	TCM.current_procedure = TCM.TrunkProcedure
end

function TCM.FinishProcedure()
	Robot.Message(TCM.TreeName.."> Finish")
	-- Return to Base
	local return_distance = 0
	while return_distance < TCM.trunk_distance do
		if Robot.MoveDown() then
			return_distance = return_distance + 1
		end
	end
	Robot.TurnLeft()
	Robot.TurnLeft()
	Robot.MoveForwardEX()
	Robot.TurnLeft()
	Robot.TurnLeft()
	TCM.tree_finished = true
end


function TCM.TrunkProcedure()
	Robot.Message(TCM.TreeName.."> Trunk Level "..tostring(TCM.trunk_distance+1))
	-- Cut branches if found
	if TCM.current_branch == 0 then
		if TCM.TestWood(Robot.Look(Robot.DIRECTION.FORWARD)) then
			TCM.current_branch = 1
			TCM.current_procedure = TCM.BranchProcedure
			return
		end
	elseif TCM.current_branch == 1 then
		Robot.TurnLeft()
		TCM.current_branch = 2
		if TCM.TestWood(Robot.Look(Robot.DIRECTION.FORWARD)) then
			TCM.current_procedure = TCM.BranchProcedure
		end
		return
	elseif TCM.current_branch == 2 then
		Robot.TurnLeft()
		TCM.current_branch = 3
		if TCM.TestWood(Robot.Look(Robot.DIRECTION.FORWARD)) then
			TCM.current_procedure = TCM.BranchProcedure
		end
		return
	elseif TCM.current_branch == 3 then
		Robot.TurnLeft()
		TCM.current_branch = 4
		if TCM.TestWood(Robot.Look(Robot.DIRECTION.FORWARD)) then
			TCM.current_procedure = TCM.BranchProcedure
		end
		return
	elseif TCM.current_branch == 4 then
		Robot.TurnLeft()
		TCM.current_branch = 0
	end

	-- Go higher up the Trunk
	if TCM.TestWood(Robot.Look(Robot.DIRECTION.UP)) then
		if Robot.Dig(Robot.DIRECTION.UP) then
			if Robot.MoveUpEX() then
				TCM.trunk_distance = TCM.trunk_distance + 1
			end
		end
	else
		TCM.current_procedure = TCM.FinishProcedure
	end
end


function TCM.BranchProcedure()
	Robot.Message(TCM.TreeName.."> Branch "..tostring(TCM.current_branch))
	-- Cut branch outward
	local branch_distance = 0
	while TCM.TestWood(Robot.Look(Robot.DIRECTION.FORWARD)) do
		if Robot.Dig(Robot.DIRECTION.FORWARD) then
			if Robot.MoveForwardEX() then
				branch_distance = branch_distance + 1
			end
		end
	end
	-- Turn around if needed
	if branch_distance > 0 then
		Robot.TurnLeft()
		Robot.TurnLeft()
	end
	-- Return To Trunk
	local return_distance = 0
	while return_distance < branch_distance do
		if Robot.MoveForwardEX() then
			return_distance = return_distance + 1
		end
	end
	-- Turn around if needed
	if branch_distance > 0 then
		Robot.TurnLeft()
		Robot.TurnLeft()
	end
	TCM.current_procedure = TCM.TrunkProcedure
end


-- REQUIRED TREECUTTER MODULE FUNCTION : Initialize(ROBOT, TREECUTTER, minecraft_id)
function TCM.Initialize(ROBOT, TREECUTTER, minecraft_id)
	if TCM.TestWood(minecraft_id) then
		Robot = ROBOT
		TreeCutter = TREECUTTER
		TCM.tree_finished = false
		TCM.current_procedure = TCM.StartProcedure
		return true
	end
	return false
end

-- REQUIRED TREECUTTER MODULE FUNCTION : Update()
function TCM.Update()
	if TCM.tree_finished == false then
		TCM.current_procedure()
		return true
	end
	return false
end

-- REQUIRED TREECUTTER MODULE FUNCTION : Display()
function TCM.Display()
	--Robot.Debug("TCM.OnDisplay()")
	return true
end

-- RETURN TREECUTTER MODULE INSTANCE
return TCM