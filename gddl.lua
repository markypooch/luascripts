local args = {...}

print("Google Drive File Download")
print("By Neometron 2018")
print("")

if #args ~= 2 then
  error("Usage: "..shell.getRunningProgram().." <GOOGLE_DRIVE_FILE_ID> <SAVE_FILE_PATH>")
end

local GOOGLE_DRIVE_FILE_ID = args[1]
local SAVE_FILE_PATH       = args[2]

local GOOGLE_DRIVE_PATH = "https://drive.google.com/uc?export=download&id="..GOOGLE_DRIVE_FILE_ID
local google_drive = http.get(GOOGLE_DRIVE_PATH)

if google_drive then
  
  local data = google_drive.readAll()

  if data then
    if not fs.exists(SAVE_FILE_PATH) then
      f = fs.open(SAVE_FILE_PATH, "w")
      f.write(data)
      f.close()
      print("File saved at: '"..SAVE_FILE_PATH.."'")
    else
      error("Error: '"..SAVE_FILE_PATH.."' already exists.")
    end
  else
    error("Error: Could not download data from '"..GOOGLE_DRIVE_PATH)
  end
  google_drive.close()  
  
 else
    error("Error: http.get failed")
end