local Robot = require "Robot"

function sendToFurnace(data, ore)
    if data.name == ore or data.name == "minecraft:coal" then
        turtle.turnLeft()
        turtle.turnLeft()
        if data.name == "minecraft:coal" then
            turtle.drop(64)
            turtle.turnRight()
            turtle.turnRight()
            
        else
            turtle.up()
            turtle.forward()
            turtle.dropDown(64)
            turtle.turnLeft()
            turtle.turnLeft()
            turtle.forward()
            turtle.down()
        end
        return True
    end
    return False
end

function sendToDepot(depotChest, data, ore)
    
        if (depotChest == "right") then
            turtle.turnRight()
            turtle.drop(64)
            turtle.turnLeft()
        else
            turtle.turnLeft()
            turtle.drop(64)
            turtle.turnRight()
        end
end

function determineItemPlacement(depotChest, data, ore)
    data = turtle.getItemDetail(turtle.getSelectedSlot())
    if data ~= nil then
        if not sendToFurnace(data, ore) and data.name ~= "" then
            sendToDepot(depotChest, data, ore)
        end
        print("Retrieved "..data.name.." from chest")
    end
    data = nil
end

function main()
    local ore = Robot.InputString("ore")
    local depotChest = Robot.InputString("Depot Chest")
    
    while 1 do
        turtle.select(9)
        data = turtle.getItemDetail(turtle.getSelectedSlot())
        if data ~= nil then
            print("Turtle retaining stale inventory, waiting to offload")
            determineItemPlacement(depotChest, data, ore)
        else
        
            turtle.suck(64)
            determineItemPlacement(depotChest, data, ore)
        end
        sleep(1)
    end        
end

main()