function string:split( inSplitPattern, outResults )
    if not outResults then
        outResults = { }
    end
    local theStart = 1
    local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
    while theSplitStart do
        table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
        theStart = theSplitEnd + 1
        theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
    end
    table.insert( outResults, string.sub( self, theStart ) )
    return outResults
end

function filter_inventory(filterList, filterListCount)
    
    for i=1, 16 do
        turtle.select(i)
        itemData = turtle.getItemDetail()
     
        if itemData ~= nil then
                
            keep = false
            for i=1, filterListCount do
                if itemData.name == filterList[i] then
                    keep = true
                end
            end
        
            if keep == false then
                turtle.drop()
            end
        end
    end
end

function fuel()
    for i=1, 16 do
        turtle.select(i)
        itemData = turtle.getItemDetail()
        
        if itemData ~= nil then        
            if itemData.name == "minecraft:coal" then
                turtle.refuel()
            end
        end
    end
end

function restful_request(url, method, body, content_type, token)
    headers = {}
    headers['Content-Type']  =content_type
    headers['content-encoding'] = "identity"
    
    response = nil
    err      = nil
    
    if (method == "GET") then
        response, err = http.get(url, headers)
    elseif (method == "PUT") then
        response, err = http.put(url, headers) 
    else
        response, err = http.post(url, body, headers)
    end
    
    filed = io.open("dump.txt", "w")
    filed:close()
    
    if (err ~= nil) then
        print("Error executing restful request")
        print(err)
        return nil
    end
            
    return response
end

function main()

    local pitch = 0
    pitch = 1
    
    local iterations = 64
    local currentIteration = 0

    local startY = 0
    local digDown = true
    local startCommand = false
    local resp         = nil
    
    local args = {}
    local filterList = {}
    local filterListCount = 0
    
    while 1 do
        print("Checking for request from Server...")
        
        if startCommand == false then
            os.sleep(5)
            local label = os.getComputerLabel()
            url = "https://qsbx2bcx1m.execute-api.us-east-1.amazonaws.com/prod/?client_id=" .. label .. ""
            resp = restful_request(url, "GET", "", "application/json", "")
        end
        
        print(resp.getResponseCode())
        if resp.getResponseCode() == 201 then
            print("No Request recieved")
            -- os.sleep(10)
        else
            
            if startCommand == false then
                str = resp.readAll()
                print(str)
                -- argsl = mysplit(resp.readAll(), ',')
                
                cc = 0
                strTable = str:split(",")
                print(#strTable)
                for i=1, #strTable do
                
                    if i == 1 then
                        filterListCount = tonumber(strTable[i])
                    else
                        filterList[i-1] = strTable[i] 
                    end
                    
                    print(strTable[i])
                    cc = cc + 1
                end
                for i=1, #filterList do
                    print(filterList[i])
                end                  
                -- print(argsl[1])
                -- filterListCount = tonumber(argsl[1])
                
                --j = 0
                --for i=2, filterListCount+1 do
                   -- j = j +1
                   -- filterList[j] = args[i]
                    
                   -- print(filterList[j])
                --end
            end
            
            startCommand = true
            if digDown then
                dd = turtle.digDown()
                md = turtle.down()
                
                if dd ~= true and md ~= true then
                    digDown = false
                    fuel()
                    filter_inventory(filterList, filterListCount)
                else
                    startY = startY - 1
                end
                
            else
                if startY < 0 then
                    turtle.digUp()
                    turtle.up()
                    startY = startY + 1
                else
                    print("Done")
                    digDown = true
                    
                    foundItems = {}
                    foundItemOffset = 0
                    for i=1, 16 do
                        turtle.select(i)
                        itemData = turtle.getItemDetail()
                        if itemData ~= nil then
                            for i=1, #filterList do
                                if itemData.name == filterList[i] then
                                    foundItems[foundItemOffset] = itemData.name
                                    foundItemOffset = foundItemOffset + 1
                                end
                            end
                        end
                    end
                    
                    running = "Not Running"
                    if currentIteration + 1 <= iterations then
                        running = "Running"
                    end
                    
                    turtleFuelLevel = turtle.getFuelLevel()
                    label = os.getComputerLabel()
                    body = '{"id":' .. label .. ',"running":"' .. running .. '", "fuel_level":' .. turtleFuelLevel .. ','
                    
                    itemList = '"items":['

                    if #foundItems > 0 then
                        for i=1, #foundItems do
                            if i < #foundItems then
                                itemList = itemList .. '"' .. foundItems[i] .. '"' .. ','
                            else 
                                itemList = itemList .. '"' .. foundItems[i] .. '"' .. ']}'
                            end
                        end
                    else
                        itemList = itemList .. '"None"]}' .. ''
                    end
                    
            
                    body = body .. itemList .. ''
                    response = restful_request('https://qsbx2bcx1m.execute-api.us-east-1.amazonaws.com/prod/report', 'POST', body, "application/json", "")  
                    
                    currentIteration = currentIteration + 1
                    if currentIteration <= iterations then
                        for i=1, pitch do
                            while not turtle.forward() do
                                turtle.dig()
                            end
                        end
                    else 
                        startCommand = false
                        currentIteration = 0
                    end              
                end
            end                
        end
    end
end

main()