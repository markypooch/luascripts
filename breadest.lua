robot = require("Robot")

local function main()

    local width   = 0
    local height  = 0
    local xOffset = 0
    
    local outputChestX = 0
    local outputChestY = 0
    local outputChestZ = 0
    
    local startingLocation = ""

    print("=====================")
    print(":                   :")
    print(":      Breadest     :")
    print(":                   :")
    print("=====================")
    print("")
    print("Enter the height of the farm: ")
    height = io.read()
    
    print("Enter the width of the farm: ")
    width = io.read()
    
    while True do 
    
        farm(0, height, width)

        returnToOrigin(height, width)

        os.sleep(4200)

        farm(1, height, width)   
    
        returnToOrigin(height, width)
    
        makeBread()
    end
end

function returnToOrigin(height, width)
    if width % 2 ~= 0 then
        for i = 1, height+1 do
            robot.MoveForward()
        end
        
        robot.TurnRight()
        
        for i = 1, width+1 do
            robot.MoveForward()
            robot.MoveForward()
        end
        robot.TurnRight()
    else
        for i = 1, height+1 do
            robot.MoveForward()
        end
        
        robot.TurnLeft()
        
        for i = 1, width+1 do
            robot.MoveForward()
            robot.MoveForward()
        end
        robot.TurnLeft()      
    end
end

function makeBread()
    while true do
       turtle.select(2)
       turtle.transferTo(9, 1)
       turtle.transferTo(10, 1)
       turtle.transferTo(11, 1)
       turtle.select(16)
       if turtle.craft(1) ~= True then
           break
       end
    end
    print("Done Crafting")
    
    turtle.dropDown()
    turtle.select(1)
end

function farm(pass, height, width)     
    robot.TurnRight()
    
    -- initial positioning
    robot.MoveForward()
    robot.MoveForward()
    robot.TurnLeft()
    
    for i = 1, width do
    
        if pass == 0 then
            tillAndPlant(height)
        else
            harvestWheat(height)
        end
        
        robot.MoveForward()
        
        if i % 2 == 0 then
            robot.TurnLeft()
            robot.MoveForward()
            robot.MoveForward()
            robot.TurnLeft()
        else
            robot.TurnRight()
            robot.MoveForward()
            robot.MoveForward()
            robot.TurnRight()
        end
    end            
end    
    
function tillAndPlant(height)
    for i = 1, height do
        robot.MoveForward()
        turtle.digDown()
        turtle.placeDown()
    end
end

function harvestWheat(height)
    for i = 1, height do
        robot.MoveForward()
        robot.digDown()
        turtle.suckDown()
    end
end

main()